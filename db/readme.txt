EXPORT
mongoexport --db winedb --collection locations --out locations.json
mongoexport --db winedb --collection specials --out specials.json
mongoexport --db winedb --collection wines --out wines.json

IMPORT
mongoimport --db winedb --collection locations --file locations.json
mongoimport --db winedb --collection specials --file specials.json
mongoimport --db winedb --collection wines --file wines.json