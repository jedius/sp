var url = require('url');

module.exports = function (env) {
  var config;
  config = {
      app: {
          url: 'http://'+process.env.IP+':'+process.env.PORT+'',
          name: 'wineCellar',
          port: 3000,
          secretString: 'secretString',
          maxAge: 3*60*60*1000
      },

      log: {
          level: 'info',
          path: __dirname+'/server.log'
      },
      db: {
          redisURL: url.parse(process.env.REDISTOGO_URL || 'redis://'+process.env.IP+':16379'),
          mongoUrl: process.env.MONGOHQ_URL || 'mongodb://admnin:admnin@widmore.mongohq.com:10010/awespecial',
          public: __dirname+'/app/public/',
          images: 'upload/'
      }, 
      validate: {
          email: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
          string: /^([a-zA-Z]){1}([a-zA-Z0-9]){0,48}$/,
          stringWithSpace: /^([a-zA-Z]){1}([ a-zA-Z0-9]){0,48}$/,
          objectId: /^([a-z0-9]){24}$/,
          password: /.{5,20}/,
          url: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/
      },
  };
  switch (env) {
    case 'production': {
      config.app.url = 'mongodb://admnin:admnin@widmore.mongohq.com:10010/awespecial';
      config.db.mongoUrl = process.env.MONGOHQ_URL;
      break;
    }
    case 'development': {
      config.db.mongoUrl = process.env.MONGOHQ_URL ||'mongodb://admnin:admnin@widmore.mongohq.com:10010/awespecial';
      break;
    }
    case 'testing': {
      config.db.mongoUrl = 'mongodb://admnin:admnin@widmore.mongohq.com:10010/awespecial';
      break;
    }
    default: {
      break;
    }
  }

  return config;
};

