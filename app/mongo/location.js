var mongoose = glob.modules.mongoose;

var location = new mongoose.Schema({
  name:          { type: "string" },
  main_logo:     { type: "object" },
  public_images: { type: "array"  },
  images:        { type: "array" },
  email:         { type: "string" },
  phone:         { type: "string" },
  address:       { type: "object" },
  mon:           { type: "string"},
  monEnd:        { type: "string"},
  tues:          { type: "string"},
  tuesEnd:       { type: "string"},
  wed:           { type: "string"},
  wedEnd:        { type: "string"},
  thur:          { type: "string"},
  thurEnd:       { type: "string"},
  fri:           { type: "string"},
  friEnd:        { type: "string"},
  sat:           { type: "string"},
  satEnd:        { type: "string"},
  sun:           { type: "string"},
  sunEnd:        { type: "string"},
  pub:           { type: "string"},
  pubEnd:        { type: "string"},
  loc:           { type: "number" },
  lat:           { type: "number" },
  url :          { type: "string" },
  factorWidth:   { type: "number" },
  factorHeight:  { type: "number" },
  imageHeight:   { type: "number" },
  imageWidth:    { type: "number" },
  category:      { type: "array" }
});

location.virtual('entity').get(function () {
  var entity = {
    _id: this._id.toString(),
    name: this.name,
    email: this.email,
    phone: this.phone,
    address: this.address,
    mon: this.mon,
    monEnd: this.monEnd,
    tues: this.tues,
    tuesEnd: this.tuesEnd,
    wed: this.wed,
    wedEnd: this.wedEnd,
    thur: this.thur,
    thurEnd: this.thurEnd,
    fri: this.fri,
    friEnd: this.friEnd,
    sat: this.sat,
    satEnd: this.satEnd,
    sun: this.sun,
    sunEnd: this.sunEnd,
    pub: this.pub,
    pubEnd: this.pubEnd,
    picture: this.picture,
    imageHeight: this.imageHeight,
    imageWidth: this.imageWidth,
    factorWidth: this.factorWidth,
    factorHeight: this.factorHeight,
    loc:this.loc,
    lat: this.lat,
    category: this.category
  };
  return entity;
});

module.exports = mongoose.model('location', location);
