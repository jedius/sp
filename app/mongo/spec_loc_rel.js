var mongoose = glob.modules.mongoose;

var spec_loc_rel = new mongoose.Schema({
  special_id:     { type: 'string' },
  location_id:    { type: 'string' }
});

spec_loc_rel.virtual('entity').get(function () {
  var entity = {
    id: this._id.toString(),
    special_id: this.special_id,
    location_id: this.location_id
  };
  return entity;
});

module.exports = mongoose.model('spec_loc_rel', spec_loc_rel);
