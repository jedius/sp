var mongoose = glob.modules.mongoose;

var user = new mongoose.Schema({
  email:          { type: "string" },
  password:       { type: "string" },
  firstName:      { type: "string" },
  lastName:       { type: "string" },
  gen:            { type: "string", "enum": ["m", "f"] },
  birth:          { type: "string" },
  city:           { type: "string" },
  country:        { type: "string" },
  confirmation:   { type: "bool", default: false },
  dt:             { type: "date", default: Date.now() },
  lm:             { type: "date", default: Date.now() }
});

user.virtual('entity').get(function () {
  var entity = {
    id: this._id.toString(),
    email: this.email,
    firstName: this.firstName,
    lastName: this.lastName,
    gen: this.gen,
    birth: this.birth,
    city: this.city,
    country: this.country,
    confirmation: this.confirmation,
    dt: this.dt,
    lm: this.lm
  };
  return entity;
});

module.exports = mongoose.model('user', user);
