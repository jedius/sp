module.exports = {
  user: require('./user'),
  wine: require('./wine'),
  rating: require('./rating'),
  location: require('./location'),
  special: require('./special'),
  spec_loc_rel: require('./spec_loc_rel')
};
