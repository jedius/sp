var mongoose = glob.modules.mongoose;

var wine = new mongoose.Schema({
  name:          { type: "string" },
  year:          { type: "number" },
  grapes:        { type: "string" },
  country:       { type: "string" },
  region:        { type: "string" },
  description:   { type: "string" },
  picture:       { type: "string" }
});

wine.virtual('entity').get(function () {
  var entity = {
    _id: this._id.toString(),
    name: this.name,
    year: this.year,
    grapes: this.grapes,
    country: this.country,
    region: this.region,
    description: this.description,
    picture: this.picture
  };
  return entity;
});

module.exports = mongoose.model('wine', wine);
