var mongoose = glob.modules.mongoose;

var special = new mongoose.Schema({
  name:          { type: "string" },
  location_id:   { type: "array"  },
  main_logo:      { type: "object" },
  images:         { type: "array"  },
  startDate:     { type: "date"   },
  endDate:       { type: "date"   },
  description:   { type: "string" },
  condition:     { type: "string" },
  mon:           { type: "string"},
  monEnd:        { type: "string"},
  tues:          { type: "string"},
  tuesEnd:       { type: "string"},
  wed:           { type: "string"},
  wedEnd:        { type: "string"},
  thur:          { type: "string"},
  thurEnd:       { type: "string"},
  fri:           { type: "string"},
  friEnd:        { type: "string"},
  sat:           { type: "string"},
  satEnd:        { type: "string"},
  sun:           { type: "string"},
  sunEnd:        { type: "string"},
  pub:           { type: "string"},
  pubEnd:        { type: "string"},
  factorWidth:   { type: "number" },
  factorHeight:  { type: "number" },
  imageHeight:   { type: "number" },
  imageWidth:    { type: "number" },
  price:         { type: "number" }
});

special.virtual('entity').get(function () {
  var entity = {
    _id: this._id.toString(),
    name: this.name,
    startDate: this.startDate,
    endDate: this.endDate,
    mon: this.mon,
    monEnd: this.monEnd,
    tues: this.tues,
    tuesEnd: this.tuesEnd,
    wed: this.wed,
    wedEnd: this.wedEnd,
    thur: this.thur,
    thurEnd: this.thurEnd,
    fri: this.fri,
    friEnd: this.friEnd,
    sat: this.sat,
    satEnd: this.satEnd,
    sun: this.sun,
    sunEnd: this.sunEnd,
    pub: this.pub,
    pubEnd: this.pubEnd,
    condition: this.condition,
    price: this.price,
    imageHeight: this.imageHeight,
    imageWidth: this.imageWidth,
    factorWidth: this.factorWidth,
    factorHeight: this.factorHeight,
    description: this.description,
    picture: this.picture
  };
  return entity;
});

module.exports = mongoose.model('special', special);
