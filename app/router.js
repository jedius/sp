var auth = require('./routes/auth');
var user = require('./routes/user');
var wines = require('./routes/wines');
var locations = require('./routes/locations');
var specials = require('./routes/specials');
var ratings = require('./routes/ratings');
var storage = require('./routes/storage');

module.exports = function (app) {

	//auth  trgtest
	app.post('/join', auth.join);
	app.post('/login', auth.login);
	app.post('/logout', auth.logout);

    //user
    app.get('/user', user.get);
    app.post('/user/:userId', user.update);

	// wine
    app.get('/winelist', wines.list)
    app.get('/winesCount', wines.count)
    app.get('/wines/p/:page', wines.list);

	//app.get('/wines', wines.list);
	app.post('/wines', wines.create);
	app.get('/wines/:id', wines.read);
	app.put('/wines/:id', wines.update);
	app.delete('/wines/:id', wines.delete);

    //specials
    app.get('/specials', specials.list)
    app.get('/locationsCount', locations.count);
    // app.get('/specials/p/:page', specials.list);
    app.post('/specials', specials.create);
    app.get('/specials/:id', specials.read);
    app.put('/specials/:id', specials.update);
    app.delete('/specials/:id', specials.delete);
    app.get('/specials/getRelatedLocationsIds/:id', specials.getRelatedLocationsIds)
    app.get('/specials/getRelatedLocations/:id', specials.getRelatedLocations)
    app.post('/specials/saveRelatedLocations/:id', specials.saveRelatedLocations)

    //locations
    app.get('/locationlist', locations.list);
    app.get('/locationsCount', locations.count);
    app.get('/locations/p/:page', locations.list);
    app.post('/locations', locations.create);
    app.get('/locations/:id', locations.read);
    app.put('/locations/:id', locations.update);
    app.delete('/locations/:id', locations.delete);
    app.get('/locations/getRelatedSpecialsIds/:id', locations.getRelatedSpecialsIds)
    app.get('/locations/getRelatedSpecials/:id', locations.getRelatedSpecials)
    app.post('/locations/saveRelatedSpecials/:id', locations.saveRelatedSpecials)

    //upload
    app.post('/storage/upload', storage.upload);
    //app.post('/specials/downloadImg/:url', specials.downloadImg)

    // app.get('/locations/addRelatedSpecial/:specId/to/:locId', locations.addRelatedSpecial)
    

	// app.get('/specials/nearby', specials.findNearby);
	// app.get('/stats/goodspecial', specials.updateGood);

	
	// app.put('/specials/:id', wine.upPic);
	// app.get('/specials/:lat:lon', wine.findAll22);

	// app.get('/locations', wine.findAllLocations);
	// app.get('/locations/:id', wine.findByIdLocations);

	// app.post('/fileUpload', function(req, res) {
	//     console.log(req.body);
	//     console.log(req.files);
	//     res.send('file upload is done.');
	// });

}
