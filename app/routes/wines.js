exports.list = function(req, res) {
    console.log('wine list');

    console.log(req.query.page);
    if (req.query.page) {
        req.body.skip = (req.query.page-1)*8
        req.body.limit = 8
    } else {
        req.body.skip = 0
        req.body.limit = 8
    }


    mongo.wine.count({}, function(err, count) {
        mongo.wine.find({}).skip(req.body.skip).limit(req.body.limit).exec(function(err, wines) {
            if (!err) {
                res.send(wines);
            } else {
                log.info(logFrom, err);
                res.send({
                    status: 500,
                    error: err
                });
            }
        });
    });

    //mongo.wine.find().exec(function(err, items) {
        //res.send(items);
    //});
};

exports.create = function(req, res) {
    delete req.body._id;
    mongo.wine.create(req.body, function(err, wine) {
        if(!err){
            console.log('wine', wine.entity, wine);
            if(wine){
                res.send(wine.entity)
            } 
        }else{
            res.send({
                code: 400,
                error: 'wine create error'
            })
        }
    })
}

    //db.collection('wines', function(err, collection) {
        //collection.insert(wine, {safe:true}, function(err, result) {
            //if (err) {
                //res.send({'error':'An error has occurred'});
            //} else {
                //console.log('Success: ' + JSON.stringify(result[0]));
                //res.send(result[0]);
            //}
        //});
    //});

exports.read = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving wine: ' + id);
    mongo.wine.findOne({_id: id}, function(err, item) {
        console.log('item', item);
        res.send(item);
    });
};

exports.update = function(req, res) {
    console.log('update');
    var id = req.params.id;
    //var wine = req.body;
    delete req.body._id;
    mongo.wine.update({_id: req.params.id}, req.body, function(err, count, info) {
        res.send()
    })



    //console.log('Updating wine: ' + id);
    //console.log(JSON.stringify(wine));
    //db.collection('wines', function(err, collection) {
        //collection.update({'_id':new BSON.ObjectID(id)}, wine, {safe:true}, function(err, result) {
            //if (err) {
                //console.log('Error updating wine: ' + err);
                //res.send({'error':'An error has occurred'});
            //} else {
                //console.log('' + result + ' document(s) updated');
                //res.send(wine);
            //}
        //});
    //});
}

exports.delete = function(req, res) {
    var id = req.params.id;
    console.log('Deleting wine: ' + id);
    mongo.wine.remove({_id: req.params.id}, function(err) {
        if(!err){
            console.log(err);
            res.send({code:200})
        }else{
            res.send({err: err})
        }
    })
}

exports.count = function(req, res) {
    console.log('wines, get count');
    mongo.wine.count({}, function(err, count) {
        console.log(count);
        res.send({count: count});
    });
}

    //db.collection('wines', function(err, collection) {
        //collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            //if (err) {
                //res.send({'error':'An error has occurred - ' + err});
            //} else {
                //console.log('' + result + ' document(s) deleted');
                //res.send(req.body);
            //}
        //});
    //});
