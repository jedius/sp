exports.list = function(req, res) {
    console.log('special list');
    if (req.session.passport.user) {
        console.log('session user', req.session.passport.user);
        if (req.params.page) {
            req.body.skip = (req.params.page-1)*8
            req.body.limit = 8
        } else {
            req.body.skip = 0
            req.body.limit = 100
        }

        mongo.special.count({}, function(err, count) {
            mongo.special.find({}).skip(req.body.skip).limit(req.body.limit).exec(function(err, specials) {
                if (!err) {
                    res.send(specials);
                } else {
                    log.info(logFrom, err);
                    res.send({
                        status: 500,
                        error: err
                    });
                }
            });
        });
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }

};

exports.create = function(req, res) {
    if (req.session.passport.user) {
        delete req.body._id;
        console.log('creat special');
        mongo.special.create(req.body, function(err, special) {
            if(!err){
                // console.log('special', special.entity, special);
                if(special){
                    res.send(special.entity)
                } 
            }else{
                console.log('error', err);
                res.send({
                    code: 400,
                    error: 'special create error'
                })
            }
        })
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }


    
}

exports.read = function(req, res) {
    if (req.session.passport.user) {
        console.log('read special');
        var id = req.params.id;
        console.log('Retrieving special: ' + id);
        mongo.special.findOne({_id: id}, function(err, item) {
            // console.log('item', item);
            res.send(item);
        });
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }

    
};

exports.update = function(req, res) {
    if (req.session.passport.user) {
        console.log('update special');
        delete req.body._id;

        console.log(req.params.id);

        mongo.special.update({_id: req.params.id}, req.body, function(err, count) {
            console.log('err', err);
            // console.log('updated', count);
            res.send(req.body)
        })

        // mongo.special.find({_id: req.params.id}, function(err, count) {
        //     console.log('err', err);
        //     // console.log('updated', count);
        // })
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }

    
}

exports.delete = function(req, res) {
    if (req.session.passport.user) {
        console.log('delete special');
        var id = req.params.id;
        console.log('Deleting special: ' + id);
        mongo.special.remove({_id: req.params.id}, function(err) {
            if(!err){
                console.log(err);
                res.send({code:200})
            }else{
                res.send({err: err})
            }
        })
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }

    
}

function _getIds (arr) {
    if (req.session.passport.user) {
        var result = []
        for (var i = arr.length - 1; i >= 0; i--) {
            result.push(arr[i].locationId);
        };
        return result;
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }

    
}

exports.getRelatedLocationsIds = function(req, res) {
    if (req.session.passport.user) {
        console.log('getRelatedLocationsIds');
        
        mongo.spec_loc_rel.find({ special_id: req.params.id },function (err, rel) {
            if(!err){
                console.log('rel', rel.length);
                var location_ids = [];
                for (var i = rel.length - 1; i >= 0; i--) {
                    location_ids.push(rel[i].location_id);
                };

                console.log('   send location_ids', location_ids);
                res.send(location_ids);
            } else {
                res.send({
                    status: 400,
                    error: err
                })
            }
        });

          
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
};


//get all related locations entities by special_id
exports.getRelatedLocations = function(req, res) {
    if (req.session.passport.user) {
        console.log('getRelatedLocations');
        var id = req.params.id;
        
        mongo.spec_loc_rel.find({ special_id: req.params.id },function (err, rel) {
            if(!err){

                console.log('rel', rel.length);

                var location_ids = [];
                for (var i = rel.length - 1; i >= 0; i--) {
                    location_ids.push(rel[i].location_id);
                };

                mongo.location.find({ _id: { $in: location_ids} },function (err, locations) {
                    if (!err) {

                        console.log('   send locations', locations.length);
                        res.send(locations);
                    } else {
                        console.log('err',err);
                        res.send({
                            status: 400,
                            error: err
                        })
                    }
                })
            } else {
                res.send({
                    status: 400,
                    error: err
                })
            }
        });

              
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
}

exports.saveRelatedLocations = function(req, res) {
    console.log('saveRelatedLocations');
    req.body.location_ids = req.body.location_ids || [];


    console.log(req.params.id , req.body.location_ids);

    if (req.params.id && req.body.location_ids) {

        if (req.session.passport.user) {

            mongo.spec_loc_rel.remove({ special_id: req.params.id },function (err, rels) {
                if (!err) {

                    var query = [];

                    for (var i = req.body.location_ids.length - 1; i >= 0; i--) {
                        query.push({ location_id: req.body.location_ids[i], special_id:req.params.id });
                    };

                    mongo.spec_loc_rel.create(query,function (err, rels) {
                        if (!err) {
                            // console.log('    created relations', rels);
                            console.log('    created relations');
                            res.send({status:200})
                        } else {
                            res.send({
                                status: 400,
                                error: err
                            })
                        }
                        // body...
                    })

                } else {
                    res.send({
                        status: 400,
                        error: err
                    })
                }
            })
            
        } else {
            console.log('non authorised');
            res.send({
                status: 403,
                error: 'non authorised'
            })
        }

    } else {
        res.send({
            status: 400,
            error: 'bad request'
        })
    }

}

// exports.addRelatedLocation = function(req, res) {
//     if (req.session.passport.user) {
//         var locId = req.params.locId;
//         var specId = req.params.specId;    

//         mongo.special.findById(specId,function(err, special) {
//             if (!err) {
//                 mongo.location.findById(locId,function(err, location) {
//                     if (!err) {

//                         if (special && location) {
//                             if (!special.location_id) special.location_id = [];

//                             if (special.location_id.indexOf(locId) == -1) {
//                                 special.location_id.push(locId);
//                                 special.save(function(err) {
//                                     if (!err) {
//                                         res.send({
//                                             status: 200
//                                         })
//                                     } else {
//                                         res.send({
//                                             status: 400,
//                                             err: 'save error'
//                                         })    
//                                     }
//                                 })
                                
//                             } else {
//                                 res.send({
//                                     status:400,
//                                     err: 'already related'
//                                 });     
//                             }
//                         } else {
//                             res.send({
//                                 status:400,
//                                 err: 'bad request'
//                             });
//                         }
                        
//                     } else {
//                         res.send({
//                             status: 400,
//                             error: err
//                         })
//                     }
//                 });
//             } else {
//                 res.send({
//                     status: 400,
//                     error: err
//                 })
//             }
//         });
//     } else {
//         console.log('non authorised');
//         res.send({
//             status: 403,
//             error: 'non authorised'
//         })
//     }

    

// }

// exports.findNearby = function(req, res) {
//     var ua = req.headers['user-agent'];
//     console.log('findAllTRY'+Date.now()+'--'+ua);
//     var lat = req.query.lat;
//     var lon = req.query.lon;
//     var radius = 50;
//     var limit = 10;
//     if (req.query.limit)
//         {
//             limit = parseInt(req.query.limit, 10);;
//         }
//     var sort = 'distance';
    
//     console.log('lat:'+lat);
//     console.log('lon:'+lon);
//     console.log('radius:'+radius);
//     console.log('limit:'+limit);
//     console.log('sort:'+sort);
    
//     var pos = new Array();
//     pos[0] = Number(lon);
//     pos[1] = Number(lat);
        
//     //Get list of locations that are near GPS    
//     db.collection('locations', function(err, collection) {
//         collection.find( { loc : { $near : pos } } ).limit(limit).toArray(function(err, items_location) {
            
//         var l = items_location.length;
//         var loc_ids = new Array();
//         for (var i = 0; i < l; i++) 
//         {
//              console.log('i:'+items_location[i]._id); 
//              loc_ids.push(""+items_location[i]._id);
//         }
        
//         //db.tweets.find({ "tweet.source" : { $in : ["Twitter for Mac", "Twitter for iPhone", "Twitter for iPad"] }}).count()
//         //Get list of specials for a set of locations    
//             db.collection('specials', function(err, collection) {
//                 collection.find({ "location_id" : {$in : loc_ids }}).toArray(function(err, items_special) {
//                     console.log('loc_ids_length:'+loc_ids.length); 
//                     console.log('items2:'+items_special.length); 
//                     console.log('err:'+err); 
                    
                   
//                    //Remove not needed special information
//                    //Add locations to the special that are relevant

                   
//                     var l = items_special.length;
//                     for (var i = 0; i < l; i++) 
//                     {
//                          var loc_arr = items_special[i].location_id;
//                           var newLocations = new Array();
//                           var closesLocation = null;
//                          for (var j = 0 ; j < loc_arr.length ; j++)
//                              {
//                                  for (var k = 0; k < items_location.length; k++) 
//                                       {
//                                           if (items_location[k]._id==loc_arr[j])
//                                           {
//                                                 console.log('index:'+loc_ids.indexOf(loc_arr[j]));
//                                                 console.log('loc_arr:'+loc_arr[j]);
                                                
//                                                 //Calculate distances to the locations
//                                                 var lat1 = parseFloat(items_location[k].loc.lat);
//                                                 var lon1 = parseFloat(items_location[k].loc.lon);
//                                                 var lat2 = parseFloat(pos[1]);
//                                                 var lon2 = parseFloat(pos[0]);
//                                                 var distance_rs = Math.round( exports.distance( lat1, lon1, lat2, lon2 ) );
                                                
//                                                 if ((distance_rs<closesLocation)||(closesLocation==null))
//                                                     {
//                                                         closesLocation = distance_rs; 
//                                                     }
//                                                 console.log("xx", lat1, lon1, lat2, lon2, distance_rs, closesLocation);
                                                
//                                                 items_location[k].distance_val = distance_rs;
//                                                 newLocations.push(items_location[k]);  
//                                           }
//                                       }
//                              }
//                              items_special[i].distance = closesLocation;
//                              items_special[i].restaurant = newLocations;
//                          delete items_special[i].location_id;
//                     }
                    
//                     res.header('Content-Type', 'application/json');
//                     res.header('Charset', 'utf-8');
//                     res.send(req.query.callback + '({"something": '+JSON.stringify(items_special)+'});'); 
//                 });
//             });
//       });
//     });
// };

// exports.findAll22 = function(req, res) {
//     var ua = req.headers['user-agent'];
//     console.log(Date.now()+'--'+ua);
//     var lat = req.query.lat;
//     var lon = req.query.lon;
//     console.log('lat'+lat);
//     console.log('lon'+lon);
//     var pos = new Array();
//     pos[0] = Number(lon);
//     pos[1] = Number(lat);
//     //dbCollection.find(geoSearchParams).limit(limit).toArray(this);
//     //db.specials.find( { loc : { $near : [50,50] } } ).limit(2)
//     db.collection('specials', function(err, collection) {
//         collection.find( { loc : { $near : pos } } ).limit(10).toArray(function(err, items) {
//             //res.send(items);
//   res.header('Content-Type', 'application/json');
//   res.header('Charset', 'utf-8')  
//   //res.send(req.query.callback + '({"something": "rather", "more": "pork", "tua": "tara"});'); 
//   res.send(req.query.callback + '({"something": '+JSON.stringify(items)+'});'); 
                
//         });
//     });
// };

// exports.updateGood = function(req, res) {
//     console.log(req.headers);
//     console.log(req.headers['X-Forwarded-For']);
//     console.log(req.headers['HTTP_X_FORWARDED']);
//     console.log(req.connection.remoteAddress);
//     var index_id = req.query.index;
//     var ip_remote = req.connection.remoteAddress;
//     var rating = [{ type: "special", index: index_id, ip: ip_remote, rating: 1}];
//     console.log('Adding wine: ' + JSON.stringify(rating));
//     db.collection('rating', function(err, collection) {
//         collection.insert(rating, {safe:true}, function(err, result) {
//             if (err) {
//                 res.send({'error':'An error has occurred'});
//             } else {
//                exports.updateRatingStats(index_id, function(e, count)
//             {
//                 console.log("ssssssssssss"+JSON.stringify(count));
//                 console.log('Success: ' + JSON.stringify(result[0]));
                
//                 res.header('Content-Type', 'application/json');
//                 res.header('Charset', 'utf-8');
//                 res.send(req.query.callback + '({"something": '+JSON.stringify({'index':index_id, 'count':count})+'});'); 
//                 exports.updateRatingStatsDB(index_id,count, function() {});
//             });
//             }
//         });
//     });
//INCLUDES THE IMPORTANT SETTINGS
    
    //CHECKS IF $id EXISTS
          //NEW ID VARIABLE, USED TO CHECK IF IT'S IN THE DATABASE
    
    //COUNTS LIKES & DISLIKES IF $id EXISTS
        //CHECKS IF USER HAS ALREADY RATED CONTENT
        //CHECKS IF USER HAS ALREADY RATED THIS ITEM
        
        //IF USER HAS ALREADY RATED
        //DELETES RATING
        //CHANGES RATING
        //INSERTS INITIAL RATING
        
        //COUNT LIKES & DISLIKES        
        //LIKE & DISLIKE IMAGES
        
        //CHECKS IF USER HAS ALREADY RATED CONTENT
               //CHECKS IF USER HAS ALREADY RATED THIS ITEM
       
        //IF SO, THE RATING WILL HAVE A SHADOW
                 //FORM & THE NUMBER OF LIKES & DISLIKES
        //EVERYTHING HERE DISPLAYED IN HTML AND THE "ratings" ELEMENT FOR AJAX

// };
