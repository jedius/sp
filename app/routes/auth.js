var passport = glob.modules.passport;

module.exports.join = function(req,res,next){ 
    console.log('join');
    passport.authenticate('local-join', function(err,user,info) {
        if (!err){
            if (user){
              
              delete req.body.username;
              delete req.body.password;
              
              mongo.user.update({_id: user.id}, req.body, function(err) {
                  if(!err){
                      req.logIn(user, function(err){
                          if (err) { 
                              return next(err); 
                          }
                      })
                      res.send({
                        code: 200,
                        data: user,
                        session: req.session.passport.user
                      });
                  }
              })
            }else{
                res.send({ 
                    code: 400,
                    error: 'email already registred'
                });}
        }else{
          res.send({ 
              code: 400,
              error: 'already registred'
          });}
    })(req,res,next);
};

module.exports.login = function(req,res,next){ 
    passport.authenticate('local-login', function(err,user,info) {
    if (!err){
        if (user){
            req.logIn(user, function(err){
                if (err) { 
                    return next(err); 
                }
            })
	          res.send({ 
	            code: 200,
	            data: user,
	            otp: false,
	            session: req.session.passport.user
	          });
        }else{
          res.send({ 
              code: 400,
              error: 'wrong login or password'
          });
        }
    }else{
      res.send({ 
          code: 400,
          error: 'login error'
      });
    }
    })(req,res,next);
};

module.exports.logout = function(req, res){
  req.logout();
  res.redirect('/');
};
