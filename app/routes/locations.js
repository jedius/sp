exports.findById = function(req, res) {

    if (req.session.passport.user) {
        var id = req.params.id;
        // console.log('Retrieving locations: ' + id);
        mongo.location.findOne({'_id': id}, function(err, item) {
            res.send(item);
        });
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
};


exports.findAllLocations = function(req, res) {

    if (req.session.passport.user) {
        db.collection('locations', function(err, collection) {
            collection.find().toArray(function(err, items) {
                res.send(items);
            });
        });
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
};

exports.list = function(req, res) {

    if (req.session.passport.user) {
        console.log(req.query.page);
        if (req.query.page) {
            req.body.skip = (req.query.page-1)*20
            req.body.limit = 20
        } else {
            req.body.skip = 0
            req.body.limit = 1000
        }

        // console.log(req.query,req.body,req.params);

        mongo.location.count({}, function(err, count) {
            mongo.location.find({}).skip(req.body.skip).limit(req.body.limit).exec(function(err, locations) {
                if (!err) {
                    res.send(locations);
                } else {
                    log.info(logFrom, err);
                    res.send({
                        status: 500,
                        error: err
                    });
                }
            });
        });
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
};

exports.create = function(req, res) {

    if (req.session.passport.user) {
        delete req.body._id;
        mongo.location.create(req.body, function(err, location) {
            if(!err){
                // console.log('location.entity', location.entity,'location', location);
                if(location){
                    res.send(location.entity)
                } 
            }else{
                res.send({
                    code: 400,
                    error: 'location create error'
                })
            }
        })
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
}

exports.read = function(req, res) {

    if (req.session.passport.user) {
        var id = req.params.id;
        console.log('Retrieving location: ' + id);
        mongo.location.findOne({_id: id}, function(err, item) {
            // console.log('item', item);
            res.send(item);
        });
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
};

exports.update = function(req, res) {

    if (req.session.passport.user) {
        var id = req.params.id;
        //var location = req.body;
        delete req.body._id;
        mongo.location.update({_id: req.params.id}, req.body, function(err, count, info) {
            res.send({_id: req.params.id})
        })
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
}

exports.delete = function(req, res) {

    if (req.session.passport.user) {
        var id = req.params.id;
        console.log('Deleting location: ' + id);
        mongo.location.remove({_id: req.params.id}, function(err) {
            if(!err){
                res.send({code:200})
            }else{
                res.send({err: err})
            }
        })
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
}

exports.count = function(req, res) {
    if (req.session.passport.user) {
        console.log('get count');
        mongo.location.count({}, function(err, count) {
            console.log(count);
            res.send({count: count});
        });
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
}

function _getIds (arr) {

    if (req.session.passport.user) {
        var result = []
        for (var i = arr.length - 1; i >= 0; i--) {
            result.push(arr[i].specialId);
        };
        return result;
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
}

exports.getRelatedSpecialsIds = function(req, res) {
    console.log('getRelatedSpecialsIds');
    if (req.session.passport.user) {
        
        
        mongo.spec_loc_rel.find({ location_id: req.params.id },function (err, rel) {
            if(!err){

                var special_ids = [];
                for (var i = rel.length - 1; i >= 0; i--) {
                    special_ids.push(rel[i].special_id);
                };

                console.log('   send special_ids', special_ids);
                res.send(special_ids);
            } else {
                res.send({
                    status: 400,
                    error: err
                })
            }
        })
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
}

exports.getRelatedSpecials = function(req, res) {
    console.log('get related specials, start');

    if (req.session.passport.user) {

        mongo.spec_loc_rel.find({ location_id: req.params.id },function (err, rel) {
            if(!err){

                var special_ids = [];
                for (var i = rel.length - 1; i >= 0; i--) {
                    special_ids.push(rel[i].special_id);
                };

                mongo.special.find({ _id: { $in: special_ids } },function (err, specials) {
                    if (!err) {
                        console.log('   send specials', specials);
                        res.send(specials);
                    } else {
                        console.log('err', err);
                        res.send({
                            status: 400,
                            error: err
                        })
                    }
                })
            } else {
                console.log('err', err);
                res.send({
                    status: 400,
                    error: err
                })
            }
        });
    } else {
        console.log('non authorised');
        res.send({
            status: 403,
            error: 'non authorised'
        })
    }
}

exports.saveRelatedSpecials = function(req, res) {
    req.body.special_ids = req.body.special_ids || [];

    console.log('save related specials, start');
    if (req.params.id && req.body.special_ids) {

        if (req.session.passport.user) {

            mongo.spec_loc_rel.remove({ location_id: req.params.id },function (err, rels) {
                if (!err) {

                    var query = [];

                    for (var i = req.body.special_ids.length - 1; i >= 0; i--) {
                        query.push({ special_id: req.body.special_ids[i], location_id:req.params.id });
                    };

                    mongo.spec_loc_rel.create(query,function (err, rels) {
                        if (!err) {
                            // console.log('    created relations', rels);
                            console.log('    created relations');
                            res.send({status:200})
                        } else {
                            res.send({
                                status: 400,
                                error: err
                            })
                        }
                    })

               } else {
                   res.send({
                       status: 400,
                       error: err
                   })
               }
           })

        } else {
            console.log('non authorised');
            res.send({
                status: 403,
                error: 'non authorised'
            })
        }

    } else {
        res.send({
            status: 400,
            error: 'bad request'
        })
    }
}

// exports.addRelatedSpecial = function(req, res) {
//     if (req.session.passport.user) {
//         var locId = req.params.locId;
//         var specId = req.params.specId;    

//         console.log(locId, specId);

//         mongo.special.findById(specId,function(err, special) {
//             if (!err) {
//                 mongo.location.findById(locId,function(err, location) {
//                     if (!err) {
//                         // console.log(special);
//                         // console.log(special.location_id);

//                         if (special && location) {
//                             if (!special.location_id) special.location_id = [];

//                             if (special.location_id.indexOf(locId) == -1) {
//                                 special.location_id.push(locId);
//                                 special.save(function(err) {
//                                     if (!err) {
//                                         res.send({
//                                             status: 200
//                                         })
//                                     } else {
//                                         res.send({
//                                             status: 400,
//                                             err: 'save error'
//                                         })    
//                                     }
//                                 })
                                
//                             } else {
//                                 res.send({
//                                     status:400,
//                                     err: 'already related'
//                                 });     
//                             }
//                         } else {
//                             res.send({
//                                 status:400,
//                                 err: 'bad request'
//                             });
//                         }

//                     } else {
//                         res.send({
//                             status: 400,
//                             error: err
//                         })
//                     }
//                 });
//             } else {
//                 res.send({
//                     status: 400,
//                     error: err
//                 })
//             }
//         });
//     } else {
//         console.log('non authorised');
//         res.send({
//             status: 403,
//             error: 'non authorised'
//         })
//     }

// }
