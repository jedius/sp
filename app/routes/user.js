exports.get = function(req, res) {
    console.log('user.get ',req.session.passport.user);
    if (req.session.passport.user) {
      mongo.user.findById(req.session.passport.user, function(err, user) {
          if (!err) {
              if (user) {
                  res.send ({
                      code: 200,
                      data: user.entity
                  });
              }else{
                  //log.warn(logFrom, 'getself user not found');
                  res.send({
                      code: 400,
                      error: 'getself user not found'
                  });
              }
          } else {
              //log.error(logFrom, 'getself user not found');
              res.send ({
                  code: 400,
                  error: 'getself user not found'
              })
          }
      });
    } else {
      console.log('non authorised');
      res.send({
          status: 403,
          error: 'non authorised'
      })

    }

};

exports.update = function(req, res) {
    console.log(req.body);
    console.log('user', req.session);
	if (req.body) {
        if(req.body.newPassword){
            req.body.password = req.body.newPassword
            mongo.user.update({ _id: req.params.userId, password:req.body.oldPassword }, req.body, function (err, count) {
                if (!err) {
                  if(count>0){
                    res.send ({
                      code: 200,
                      message: 'updated'
                    });
                  }else{
                    res.send ({
                        code: 400,
                        error: 'Uncorrect password'
                    })
                  }
                }else{
                  log.error(logFrom, 'user update error')
                  res.send({
                    code: 400,
                    error: 'user find error'
                  });
                }
            });
        }else{
            mongo.user.update({ _id: req.params.userId }, req.body, function (err, count) {
                if (!err) {
                  if(count>0){
                    res.send ({
                      code: 200,
                      message: 'updated'
                    });
                  }else{
                    res.send ({
                        code: 400,
                        error: 'nothing to update'
                    })
                  }
                }else{
                  log.error(logFrom, 'user update error')
                  res.send({
                    code: 400,
                    error: 'user find error'
                  });
                }
            });
        }
	}else{
		log.error(logFrom, 'params not found');
		res.send ({
			code: 400,
			error: 'params not found'
		})
	}
};

