var AppRouter = Backbone.Router.extend({

    routes: {
        ""                  :        "home",
        "wines"	: "list",
        "wines/page/:page"  :        "list",
        "specials":                  "listSpecials",
        "specials/page/:page"  :     "listSpecials",
        "locations":                 "listLocations",
        "locations/addLocations":    "addLocations",
        "locations/page/:page"  :    "listLocations",
        "wines/add"         :        "addWine",
        "wines/addSpecials" :        "addSpecials",
        "wines/:id"         :        "wineDetails",
        "locations/:id"     :        "locationDetails",
        "locations/:id/setupimages":   "setupimages",
        "specials/:id"      :        "specialsDetails",
        "specials/:id/setlogo":      "setlogo",
        "passwordReset"     :        "passwordReset",
        "about"             :        "about",
        "signin"            :        "signin",
        "signup"            :        "signup",        
        editProfile         :        "editProfile",
        "logout"            :        "logout"
    },

    initialize: function () {
        this.headerView = new HeaderView();
        this.headerView.render();

        //$('.header').html(this.headerView.el);
    },

    home: function (id) {
        if (!this.homeView) {
            this.homeView = new HomeView();
        }
        $('#content').html(this.homeView.el);
        //this.headerView.selectMenuItem('home-menu');
    },

    // pickLogo: function (specialId) {
    //     
    // },

    setlogo: function (id) {
        console.log('set logo');
        if (!window.specialList) {
            window.specialList = new window.SpecialCollection();
        }
        window.specialList.fetch({
            success: function(){

                // $("#content").html(new SpecialListView({collection: window.specialList, page: p, count:window.specialList.length}).el);
                console.log('window.specialList:', id, window.specialList);
                var model = window.specialList.each(function(model, i) {
                    if (model.id === id){
                        console.log('model', model);
                        $('#content').html(new ImageListView({model: model}).el);  
                    }
                })
            }
        });
    },

    setupimages: function (id) {
        console.log('set images');
        if (!window.locationList) {
            window.locationList = new window.LocationCollection();
        }
        window.locationList.fetch({
            success: function(){
                console.log(id, window.locationList);
                var model = window.locationList.each(function(model, i) {
                    if (model.id === id) $('#content').html(new ImageListView({model: model}).el);
                })
            }
        });
    },

	listSpecials: function(page) {
        console.log('listSpecials', page);
        var p = page ? parseInt(page, 10) : 1;
        if (!window.specialList) {
            window.specialList = new window.SpecialCollection();
        }
        window.specialList.fetch({
            data: $.param({ page: p}),
            success: function(){
                $("#content").html(new SpecialListView({collection: window.specialList, page: p, count:window.specialList.length}).el);
            }
        });
        this.headerView.selectMenuItem('specials-menu');
    },

    listLocations: function(page) {
        console.log('listLocations', page);
        var p = page ? parseInt(page, 10) : 1;

        if (!window.locationList) {
            window.locationList = new window.LocationCollection();
        }
        
        window.locationList.getCount(function(count) {
            window.locationList.fetch({
                data: $.param({ page: p }),
                success: function(){
                    console.log('3', window.locationList);
                    $("#content").html(new LocationListView({collection: window.locationList, page: p, count:count}).el);
                }
            });
            
        })
        this.headerView.selectMenuItem('locations-menu');
    },
    
	list: function(page) {
        // var p = page ? parseInt(page, 10) : 1;
        // var wineList = new window.WineCollection();
        // wineList.fetch(p, function(count){
        //     $("#content").html(new WineListView({model: wineList, page: p, count:count}).el);
        // });
        // this.headerView.selectMenuItem('wine-menu');

        var self = this

        console.log('wineList', page);
        var p = page ? parseInt(page, 10) : 1;

        if (!window.wineList) {
            window.wineList = new window.WineCollection();
        }
        
        window.wineList.getCount(function(count) {
            
            window.wineList.fetch({
                data: $.param({ page: p }),
                success: function(){
                    console.log('3', window.wineList);
                    $("#content").html(new WineListView({collection: window.wineList, page: p, count:count}).el);
                }
            });
            
        })
        this.headerView.selectMenuItem('wine-menu');
    },

    wineDetails: function (id) {
        var wine = new Wine({_id: id});
        console.log('wine ', wine);
        wine.fetch({success: function(lol){
            console.log('wine ', lol);
            $("#content").html(new WineView({model: wine}).el);
        }});
        this.headerView.selectMenuItem();
    },
    
    locationDetails: function (id) {
        console.log('router open location');
        var location = new Location({ _id: id });
        location.fetch({success: function(){
            console.log('location ready');
            $("#content").html(new LocationsView({model: location}).el);
        }});

        this.headerView.selectMenuItem();
    },


    specialsDetails: function (id) {
        console.log('specialsDetails');

        var special = new Special({ _id: id });
        
        special.fetch({success: function(){
             console.log('add content to view');
             $("#content").html(new SpecialsView({model: special}).el);   
        }});

        // special.fetch({
        //     success: function(){
        //         $("#content").html(new SpecialsView({model: special}).el);
        //     }
        // })
        this.headerView.selectMenuItem();
    },


	addWine: function() {
        var wine = new Wine();
        $('#content').html(new WineView({model: wine}).el);
        this.headerView.selectMenuItem('addwines-menu');
	},	
        
        
    addSpecials: function() {
        console.log('addSpecials');
        var special = new Special({});
        console.log('special', special.set({images:[]}));
        $('#content').html(new SpecialsView({model: special}).el);
        this.headerView.selectMenuItem('addspecials-menu');
	},

    addLocations:function() {
        var location = new Location();
        $('#content').html(new LocationsView({model: location}).el);
        this.headerView.selectMenuItem('addlocation-menu');
    },

    about: function () {
        if (!this.aboutView) {
            this.aboutView = new AboutTest();
        }
        $('#content').html(this.aboutView.el);
        this.headerView.selectMenuItem('about-menu');
    },

    signin: function () {
        if (!this.signinView) {
            this.signinView = new SignInView();
        }
        $('#content').html(this.signinView.el);
        this.headerView.selectMenuItem('signin-menu');
    },


    signup: function () {
        if (!this.signupView) {
            this.signupView = new SignUpView();
        }
        this.signupView.render();
        $('#content').html(this.signupView.el);
        this.headerView.selectMenuItem('signup-menu');
    },

    passwordReset: function() {
        if (!this.passwordResetView) {
            this.passwordResetView = new PasswordResetView();
        }
        this.passwordResetView.render();
        $('#content').html(this.passwordResetView.el);
    },

    editProfile: function() {
        if (!this.editProfileView) {
            this.editProfileView = new EditProfileView();
        }
        this.editProfileView.render();
        $('#content').html(this.editProfileView.el);
    },

    passwordReset: function() {
        if (!this.passwordResetView) {
            this.passwordResetView = new PasswordResetView();
        }
        this.passwordResetView.render();
        $('#content').html(this.passwordResetView.el);
    },


    logout:function() {
        window.user.logOut(function() {
            app.headerView.render();
            app.navigate('/', {trigger: true} )
        })
    }
});

utils.loadTemplate(['HomeView', 'ImagesView', 'ImageListView', 'ImageListItemView', 'HeaderView', 'WineView', 'WineListItemView', 'AboutView', 'EditProfileView', 'SignInView', 'SignUpView', 'PasswordResetView', 'AboutTest', 'SpecialsView', 'SpecialListItemView', 'LocationsView','LocationListItemView'], function() {
    window.user = new window.UserModel()
    window.user.findSelf(function() {
        app = new AppRouter();
        Backbone.history.start();
    })
});
