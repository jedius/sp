window.WineCollection = Backbone.Collection.extend({

    model: window.Wine,

    url: "/winelist",

    getCount: function (cb) {
        $.ajax({
            url:'/winesCount',
            type:'GET',
            error:function(err) {

            },
            success:function(body, message, xhr){
                console.log(arguments);
                console.log('res wine get count', body);
                console.log(xhr.statusCode);
                switch(xhr.status){
                    case 200:
                        cb(body.count)
                        break;

                    case 400:
                        cb(body.error)
                        break;

                    default:
                        console.log('body');
                        //cb(body.error)
                        break;
                }  
            }
        })
    },

});