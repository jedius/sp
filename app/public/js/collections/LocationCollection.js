window.LocationCollection = Backbone.Collection.extend({

    model: window.Location,

    url: "/locationlist",

    initialize: function() {
        console.log(window.Location);
        // this.model = window.Location
    },

    getCount: function (cb) {
        $.ajax({
            url:'/locationsCount',
            type:'GET',
            error:function(err) {

            },
            success:function(body, message, xhr){
                console.log(arguments);
                console.log('res location get count', body);
                console.log(xhr.statusCode);
                switch(xhr.status){
                    case 200:
                        cb(body.count)
                        break;

                    case 400:
                        cb(body.error)
                        break;

                    default:
                        console.log('body');
                        //cb(body.error)
                        break;
                }  
            }
        })
    }

});