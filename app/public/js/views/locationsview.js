window.LocationsView = Backbone.View.extend({

    events: {
        "change"        : "change",
        "click .save"   : "save",
        "click .delete" : "deleteWine",
        "click #save-img": "selectImageArea",
        "drop #picture" : "dropHandler",
        "dragover #picture" : "dragoverHandler",
        "click #save-changes-links" : "AcceptRelatedSpecials"
    },

    initialize: function () {
        console.log('init 1');
        var self = this
        this.model._getRelatedSpecialsIds({
            success: function () {
                console.log('init 2');
                self.model.relatedSpecials = new window.SpecialLinkCollection();
                console.log('init related specials', self.model.relatedSpecials);
                self.model.relatedSpecials.fetchId = self.model.get('_id');

                self.render();
            }
        });
    },

    render: function () {
        console.log('render');

        console.log('this.model.toJSON()', this.model.toJSON());
        $(this.el).html(this.template(this.model.toJSON()));
        
        $('#specialsModal').modal();

        this.updateGrids();

        return this;
    },

    updateGrids: function () {
        this.$('.bbGrid-specials').html('');
        this.$('.bbGrid-add-specials').html('');

        console.log('related specials', this.model.relatedSpecials);

        this.RelatedGrid = new bbGrid.View({        
            container: this.$('.bbGrid-specials'),        
            collection: this.model.relatedSpecials,
            autofetch: true,
            colModel: [{ title: 'ID', name: 'special_id', sorttype: 'number', filter: 'true', filterType: 'input' },
                       { title: 'Full Name', name: 'name', filter: 'true', filterType: 'input' },
                       { title: 'Description', name: 'description', filter: 'true', filterType: 'input'  },
                       { title: 'Conditions', name: 'conditions', filter: 'true', filterType: 'input'  } ],
            rows: 25,
            rowList: [25,50, 100]
        });

        if (!window.specialList) {
            window.specialList = new window.SpecialCollection(); 
        };

        console.log(window.specialList);

        this.AddGrid = new bbGrid.View({        
            container: this.$('.bbGrid-add-specials'),        
            collection: window.specialList,
            multiselect: true,
            selectedRows: this.model.get('special_id'),
            autofetch: true,
            colModel: [{ title: 'ID', name: 'special_id', sorttype: 'number', filter: 'true', filterType: 'input' },
                       { title: 'Full Name', name: 'name', filter: 'true', filterType: 'input' },
                       { title: 'Description', name: 'description', filter: 'true', filterType: 'input'  },
                       { title: 'Conditions', name: 'conditions', filter: 'true', filterType: 'input'  } ]
        });
        
    },

    AcceptRelatedSpecials: function (e) {
        // console.log(this.AddGrid.selectedRows);
        // console.group('AcceptRelatedLocations');
        // console.log('model before', this.model);
        console.log('1', 'set rows', this.AddGrid.selectedRows);
        this.model.setRelatedSpecials(this.AddGrid.selectedRows);
        // console.log('model after', this.model);
        // console.groupEnd();
        this.updateGrids();
        $('#specialsModal').modal('hide');

    },

    change: function (event) {
        // Remove any existing alert message
        utils.hideAlert();

        // Apply the change to the model
        var target = event.target;
        var change = {};
        change[target.name] = target.value;
        this.model.set(change);

        // Run validation rule (if any) on changed item
        var check = this.model.validateItem(target.id);
        if (check.isValid === false) {
            utils.addValidationError(target.id, check.message);
        } else {
            utils.removeValidationError(target.id);
        }
    },

    save: function (e) {
        var self = this;
        var check = this.model.validateAll();
        if (check.isValid === false) {
            utils.displayValidationErrors(check.messages);
            return false;
        }

        this.model.save(null, {
            success: function (model) {
                app.navigate('locations/' + model.get('_id'), true);
                utils.showAlert('Success!', 'Wine saved successfully', 'alert-success');
            },
            error: function () {
                utils.showAlert('Error', 'An error occurred while trying to delete this item', 'alert-error');
            }
        });
        e.preventDefault()
        return false;
    },

    deleteWine: function () {
        this.model.destroy({
            success: function () {
                alert('Wine deleted successfully');
                window.history.back();
            }
        });
        return false;
    },

    dropHandler: function (event) {
            // console.log('dropHandler');

            var self = this;

            event.stopPropagation();
            event.preventDefault();
            var e = event.originalEvent;
            e.dataTransfer.dropEffect = 'copy';
            this.pictureFile = e.dataTransfer.files[0];

            var reader = new FileReader();
            reader.onloadend = function () {
                // console.log(reader);
                self.$('.picture').attr('src', reader.result);
                console.log('result', reader.result);
                // console.log('going to upload');
                self.uploadFile(reader.result, self.pictureFile)
                
            };
            reader.readAsDataURL(this.pictureFile);
        },

        dragoverHandler: function(event) {
            console.log('dragoverHandler');        
            event.preventDefault();
        },
        
        // upload JPEG files
    uploadFile: function(blob, file) {
            var self = this
            if (location.host.indexOf("sitepointstatic") >= 0) return

            if (file.type == "image/jpeg" || file.type == "image/png") {
                function progressHandlingFunction () {
                    // console.log('progress',arguments);
                }

                // console.log(file);

                $.ajax({
                    url: '/storage/upload',
                    type: 'POST',
                    data:  {
                        data: blob,
                        file: JSON.stringify(file)
                    },
                    success: function(res) {
                        // console.log('res',res);


                        if (res && res.url) {
                            console.log('res', res);
                            console.log('modal', $('#imagesModal'));
                            self.$('#imagesModal').modal('show')
                            self.$('.images-thumb').html('<img id="logo" src='+res.url+'></img>')
                            self.$('#logo').imgAreaSelect({ aspectRatio: '4:3', handles: true, onSelectEnd: function (img, selection) {
                                self.selection = selection
                                console.log(self.selection, 'selection');

                            } });    // work example

                            self.model.setLogo(res.url);
                            self.model.addImg(res.url);
                            console.log('model please', self.model);
                            // self.$('.picture').attr('src', res.url);
                        }
                }
            });
    
            };


    },


    selectImageArea: function () {
        console.log($('.imgareaselect-outer').parent());
        $('.imgareaselect-outer').hide()
        $('.imgareaselect-selection').parent().hide()

        if(this.selection){
            this.model.set({'main_logo':{'url':this.model.get('main_logo').url, x1:this.selection.x1, y1:this.selection.y1, x2:this.selection.x2, y2:this.selection.y2}})
            // this.$('.picture').attr('src', res.url);
        
            var y = this.selection.y2-this.selection.y1
            var x = this.selection.x2-this.selection.x1
            var factorWidth = 160/x
            var factorHeight = 120/y

            this.model.save({factorWidth: factorWidth, factorHeight:factorHeight, imageWidth: this.$('#logo').width(), imageHeight: this.$('#logo').height()})

            console.log('this.model', this.model);

            this.$('.imgDiv').css({height: 120, width: 160})

            this.$('#picture').attr('src', this.model.get('main_logo').url);

            this.$('#picture').css({width:this.$('#logo').width()*factorWidth, height:this.$('#logo').height()*factorHeight, top:0-this.selection.y1-((this.selection.y1*factorHeight-this.selection.y1)).toFixed(0), left: 0-this.selection.x1-((this.selection.x1*factorWidth-this.selection.x1)).toFixed(0)})
        }else{
            this.$('#picture').attr('src', this.model.get('main_logo').url);
        }
        this.$('#imagesModal').modal('hide')
    },




});
