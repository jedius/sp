window.SignInView = Backbone.View.extend({
	events:{
		'submit form.signin': 'signIn'
	},
    
    initialize:function () {
        this.render();
    },

    render:function () {
        $(this.el).html(this.template('SignInView'));
        return this;
    },

    signIn: function (e) {
        var data = this.$('.signin').serializeObject()
        console.log('signin, serialize', data);
        window.user.login(data, function(err) {
            if(!err){
                //utils.showAlert('Success!', 'Login success', 'alert-success');
                app.navigate('/', {trigger: true});
                app.headerView.render()
            }else{
                utils.showAlert('Error', err, 'alert-error');
                //this.$('.errLogin').text(err)
            }
        })
    	e.preventDefault()
    	return false
    }
});
