window.WineListView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

    render: function () {
        // var wines = this.model.models;
        // var len = wines.length;
        // var startPos = (this.options.page - 1) * 8;
        // var endPos = Math.min(startPos + 8, len);
        // var count = Math.ceil(this.options.count/8)

        // $(this.el).html('<ul class="thumbnails"></ul>');
        // for (var i = 0; i < wines.length; i++) {
        //     $('.thumbnails', this.el).append(new WineListItemView({model: wines[i]}).render().el);
        // }

        // $(this.el).append(new Paginator({model: this.model, page: this.options.page, count: count, entity: 'wines' }).render().el);

        // return this;

        console.log('coll', this.collection);
        var self = this

        var startPos = (this.options.page - 1) * 8;
        var endPos = Math.min(startPos + 8, this.collection.length);
        var count = Math.ceil(this.options.count/8)
        $(this.el).html('<ul class="thumbnails"></ul>');

        this.collection.each(function(model, i) {
            console.log(model,i);
            // model.set('picture', 'default.jpg')
            $('.thumbnails', self.el).append(new WineListItemView({model: model}).render().el);
            // $('.thumbnails', this.el).append(new LocationListItemView({model: locations[i]}).render().el);
        })

        $(this.el).append(new Paginator({collection: this.collection, page: this.options.page, count: count, entity: 'wines' }).render().el);

        return this;

    }
});

window.WineListItemView = Backbone.View.extend({

    tagName: "li",

    initialize: function () {
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render: function () {
        console.log(this.model.toJSON());
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }

});
