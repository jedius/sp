window.LocationListView = Backbone.View.extend({

    initialize: function () {
        console.log('LOCATION');
        this.render();
    },

    render: function () {
        console.log('coll', this.collection);
        var self = this

        var startPos = (this.options.page - 1) * 50;
        var endPos = Math.min(startPos + 50, this.collection.length);
        var count = Math.ceil(this.options.count/20);
        $(this.el).html('<ul class="thumbnails"></ul>');

        this.collection.each(function(model, i) {
            // console.log(model,i);
            // model.set('picture', 'default.jpg')
            $('.thumbnails', self.el).append(new LocationListItemView({model: model}).render().el);
            // $('.thumbnails', this.el).append(new LocationListItemView({model: locations[i]}).render().el);
        })

        $(this.el).append(new Paginator({collection: this.collection, page: this.options.page, count: count, entity: 'locations' }).render().el);

        return this;

        
        // $(this.el).html('<ul class="thumbnails"></ul>');
        // console.log('locations', locations);
        // for (var i = 0; i < locations.length; i++) {
        //   //  $('.thumbnails', this.el).append('ddd');
        //   $('.thumbnails', this.el).append(new LocationListItemView({model: locations[i]}).render().el);
        // }

        // $(this.el).append(new Paginator({model: this.model, page: this.options.page, count: count, entity: 'locations' }).render().el);

        // return this;
    }
});

window.LocationListItemView = Backbone.View.extend({

    tagName: "li",

    initialize: function () {
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});
