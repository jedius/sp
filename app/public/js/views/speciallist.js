window.SpecialListView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

    render: function () {
        // console.log('coll', this.collection);
        var self = this

        var count = Math.ceil(this.options.count/8)

        $(this.el).html('<ul class="thumbnails"></ul>');

        this.collection.each(function(model, i) {
            $('.thumbnails', self.el).append(new SpecialListItemView({model: model}).render().el);
        })

        $(this.el).append(new Paginator({collection: this.collection, page: this.options.page, count: count, entity: 'specials' }).render().el);

        return this;
    }
});

window.SpecialListItemView = Backbone.View.extend({

    tagName: "li",

    initialize: function () {
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render: function () {
        console.log('this.model.toJSON()', this.model.toJSON());
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }

});
