window.EditProfileView = Backbone.View.extend({

    events:{
        'click #edit': 'update'
    },

    initialize: function () {
        this.render()
    },

    render: function () {
        $(this.el).html(this.template({user:window.user.toJSON()}));
        return this;
    },

    selectMenuItem: function (menuItem) {
        $('.nav li').removeClass('active');
        if (menuItem) {
            $('.' + menuItem).addClass('active');
        }
    },

    update: function() {
        var data = this.$('.edit-form').serializeObject()
        if(data.newPassword == this.$('.confirmPassword').val()){
            window.user.update(data, function(err) {
                if(!err){
                    utils.showAlert('Success!', 'Update success', 'alert-success');
                }else{
                    utils.showAlert('Error', err, 'alert-error');
                }
            })
        }else{
            utils.showAlert('Error', 'error password confirmation', 'alert-error');
        }
    }
});

