window.WineView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    },

    events: {
        "change"        : "change",
        "click .save"   : "save",
        "click .delete" : "deleteWine",
        "drop #picture" : "dropHandler",
        "dragover #picture" : "dragoverHandler"
    },

    change: function (event) {
        // Remove any existing alert message
        utils.hideAlert();

        // Apply the change to the model
        var target = event.target;
        var change = {};
        change[target.name] = target.value;
        this.model.set(change);

        // Run validation rule (if any) on changed item
        var check = this.model.validateItem(target.id);
        if (check.isValid === false) {
            utils.addValidationError(target.id, check.message);
        } else {
            utils.removeValidationError(target.id);
        }
    },

    save: function (e) {
        var self = this;
        var check = this.model.validateAll();
        if (check.isValid === false) {
            utils.displayValidationErrors(check.messages);
            return false;
        }

        console.log('before save');
        console.log('wine', this.model);
        this.model.save(null, {
            success: function (model) {
                app.navigate('wines/' + model.get('_id'), true);
                utils.showAlert('Success!', 'Wine saved successfully', 'alert-success');
            },
            error: function () {
                utils.showAlert('Error', 'An error occurred while trying to delete this item', 'alert-error');
            }
        });

        e.preventDefault()
        return false;
    },

    deleteWine: function () {
        this.model.destroy({
            success: function () {
                alert('Wine deleted successfully');
                window.history.back();
            }
        });
        return false;
    },

    dropHandler: function (event) {
            // console.log('dropHandler');

            var self = this;

            event.stopPropagation();
            event.preventDefault();
            var e = event.originalEvent;
            e.dataTransfer.dropEffect = 'copy';
            this.pictureFile = e.dataTransfer.files[0];

            var reader = new FileReader();
            reader.onloadend = function () {
                // console.log(reader);
                self.$('.picture').attr('src', reader.result);
                console.log('result', reader.result);
                // console.log('going to upload');
                self.uploadFile(reader.result, self.pictureFile)
                
            };
            reader.readAsDataURL(this.pictureFile);
        },

        dragoverHandler: function(event) {
            // console.log('dragoverHandler');        
            event.preventDefault();
        },
        
        // upload JPEG files
        uploadFile: function(blob, file) {
            console.log('uploadFile', blob, file);
            // following line is not necessary: prevents running on SitePoint servers
            var self = this
            if (location.host.indexOf("sitepointstatic") >= 0) return

            if (file.type == "image/jpeg" || file.type == "image/png") {
                function progressHandlingFunction () {
                    // console.log('progress',arguments);
                }

                // console.log(file);

                $.ajax({
                    url: '/storage/upload',
                    type: 'POST',
                    data:  {
                        data: blob,
                        file: JSON.stringify(file)
                    },
                    success: function(res) {
                        // console.log('res',res);
                        if (res && res.url) {
                            self.$('.picture').attr('src', res.url);
                        }
                    }
                });
        
            };

        }

});
