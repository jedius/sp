window.Paginator = Backbone.View.extend({

    className: "pagination pagination-centered",

    initialize:function () {
        this.collection.bind("reset", this.render, this);
        this.render();
    },

    render:function () {

        var items = this.collection.models;
        var len = items.length;
        // console.log('this.count', this.options.count);
        var pageCount = this.options.count

        $(this.el).html('<ul />');
        // console.log('pageCount', pageCount);
        for (var i=0; i < pageCount; i++) {
            $('ul', this.el).append("<li" + ((i + 1) === this.options.page ? " class='active'" : "") + "><a href='#"+this.options.entity+"/page/"+(i+1)+"'>" + (i+1) + "</a></li>");
        }

        return this;
    }
});



window.PaginatorLoc = Backbone.View.extend({

    className: "pagination pagination-centered",

    initialize:function () {
        this.collection.bind("reset", this.render, this);
        this.render();
    },

    render:function () {

        var items = this.collection.models;
        var len = items.length;
        var pageCount = Math.ceil(len / 50);

        $(this.el).html('<ul />');

        for (var i=0; i < pageCount; i++) {
            $('ul', this.el).append("<li" + ((i + 1) === this.options.page ? " class='active'" : "") + "><a href='#locations/page/"+(i+1)+"'>" + (i+1) + "</a></li>");
        }

        return this;
    }
});
