window.SpecialsView = Backbone.View.extend({

    events: {
        "change"                : "change",
        "click .save"           : "save",
        "click #save-img"        : "selectImageArea",
        "click .delete"         : "deleteSpecial",
        // "click #setup-images"   : "setupImages",
        "drop .picture"         : "dropHandler",        
        "dragover .picture"     : "dragoverHandler",
        "click #save-changes-links" : "AcceptRelatedLocations"
    },

    initialize: function () {
        this.page = 0;
        console.log('new this.model', this.model);
        console.log('SpecialsView init');
        var self = this;
        this.model._getRelatedLocationsIds({
            success: function () {
                // console.log('init 2');
                self.model.relatedLocations = new window.LocationLinkCollection();
                // console.log('init related Locations', self.model.relatedLocations);
                self.model.relatedLocations.fetchId = self.model.get('_id');

                self.render();
            }
        });
    },

    render: function () {
        // console.log('specialsView render');
        


        $(this.el).html(this.template(this.model.toJSON()));

        $('#specialsModal').modal();

        this._initDatepicker();
        


        this.update();

        return this;
    },

    update: function (argument) {
        // console.log('update special view', this.model);

        this.$('.bbGrid-locations').html('')
        this.$('.bbGrid-add-locations').html('')

        this.RelatedGrid = new bbGrid.View({        
            container: this.$('.bbGrid-locations'),        
            collection: this.model.relatedLocations,
            autofetch: true,
            colModel: [{ title: 'ID', name: 'external_id', sorttype: 'number', filter: 'true', filterType: 'input' },
                       { title: 'Full Name', name: 'name', filter: 'true', filterType: 'input' },
                       { title: 'Phone', name: 'phone', filter: 'true', filterType: 'input' },
                       { title: 'Email', name: 'email', filter: 'true', filterType: 'input' } ],
            rows: 25,
            rowList: [25,50, 100]
        });

        if (!window.locationlList) {
            window.locationlList = new window.LocationCollection(); 
        }

        this.AddGrid = new bbGrid.View({        
            container: this.$('.bbGrid-add-locations'),        
            collection: window.locationlList,
            selectedRows: this.model.get('location_id'),
            autofetch: true,
            colModel: [{ title: 'ID', name: 'external_id', sorttype: 'number', filter: 'true', filterType: 'input' },
                       { title: 'Full Name', name: 'name', filter: 'true', filterType: 'input' },
                       { title: 'Phone', name: 'phone', filter: 'true', filterType: 'input' },
                       { title: 'Email', name: 'email', filter: 'true', filterType: 'input' } ],
            multiselect: true,
            rows: 14
            // rowList: [25,50, 100]
        });
        //this.$('.picture').imgAreaSelect({ aspectRatio: '4:3', handles: true });    // work example
    },

    AcceptRelatedLocations: function (e) {
        console.log(this.AddGrid.selectedRows);
        console.group('AcceptRelatedLocations');
        console.log('model before', this.model);
        this.model.setRelatedLocations(this.AddGrid.selectedRows);
        console.log('model after', this.model);
        console.groupEnd();
        this.update();
        $('#locationsModal').modal('hide');

    },

    change: function (event) {
        // Remove any existing alert message
        console.log('change');
        utils.hideAlert();

        // Apply the change to the model
        var target = event.target;
        var change = {};
        change[target.name] = target.value;
        this.model.set(change);

        // Run validation rule (if any) on changed item
        var check = this.model.validateItem(target.id);
        if (check.isValid === false) {
            utils.addValidationError(target.id, check.message);
        } else {
            utils.removeValidationError(target.id);
        }
    },

    save: function (e) {
        console.log('save', this.model.get('_id'));
        var self = this;
        var check = this.model.validateAll();
        if (check.isValid === false) {
            utils.displayValidationErrors(check.messages);
            return false;
        }

        console.log('attrs before save', this.model.attributes);
        this.model.save(null, {
            success: function (model) {
                app.navigate('specials/' + model.get('_id'), true);
                utils.showAlert('Success!', 'Wine saved successfully', 'alert-success');
            },
            error: function (err) {
                // console.log('error saveWine', err);
                utils.showAlert('Error', 'An error occurred while trying to delete this item', 'alert-error');
            }
        });

        e.preventDefault()
        return false;
    },

    deleteSpecial: function () {
        this.model.destroy({
            success: function () {
                alert('Wine deleted successfully');
                window.history.back();
            }
        });
        return false;
    },

    dropHandler: function (event) {
        console.log('dropHandler');

        var self = this;

        event.stopPropagation();
        event.preventDefault();
        console.log('this.model', this.model);
        if(this.model.get('_id')){
            var e = event.originalEvent;
            e.dataTransfer.dropEffect = 'copy';
            this.pictureFile = e.dataTransfer.files[0];

            var reader = new FileReader();
            reader.onloadend = function () {
                // console.log(reader);
                // self.$('.picture').attr('src', reader.result);
                console.log('result', reader.result);
                // console.log('going to upload');
                self.uploadFile(reader.result, self.pictureFile)
                
            };
            reader.readAsDataURL(this.pictureFile);
        }else{
            alert('You can add a image, just after the save specials')
        }
    },

    dragoverHandler: function(event) {
        console.log('dragoverHandler!!!!!!!!');        
        event.preventDefault();
    },
    
	// upload JPEG files
    uploadFile: function(blob, file) {
        console.log('uploadFile', blob, file);
		// following line is not necessary: prevents running on SitePoint servers
        var self = this
        if (location.host.indexOf("sitepointstatic") >= 0) return

        if (file.type == "image/jpeg" || file.type == "image/png") {
            function progressHandlingFunction () {
                // console.log('progress',arguments);
            }

            // console.log(file);

            $.ajax({
                url: '/storage/upload',
                type: 'POST',
                data:  {
                    data: blob,
                    file: JSON.stringify(file)
                },
                success: function(res) {
                    // console.log('res',res);


                    if (res && res.url) {
                        console.log('res', res);
                        console.log('modal', $('#imagesModal'));
                        self.$('#imagesModal').modal('show')
                        self.$('.images-thumb').html('<img id="logo" src='+res.url+'></img>')
                        self.$('#logo').imgAreaSelect({ aspectRatio: '4:3', handles: true, onSelectEnd: function (img, selection) {
                            self.selection = selection
                            console.log(self.selection, 'selection');

                        } });    // work example

                        self.model.setLogo(res.url);
                        self.model.addImg(res.url);
                        console.log('model please', self.model);
                        // self.$('.picture').attr('src', res.url);
                    }
                }
            });
    
        };

	},

    selectImageArea: function () {
        console.log($('.imgareaselect-outer').parent());
        $('.imgareaselect-outer').hide()
        $('.imgareaselect-selection').parent().hide()

        if(this.selection){
            this.model.set({'main_logo':{'url':this.model.get('main_logo').url, x1:this.selection.x1, y1:this.selection.y1, x2:this.selection.x2, y2:this.selection.y2}})
            // this.$('.picture').attr('src', res.url);
        
            var y = this.selection.y2-this.selection.y1
            var x = this.selection.x2-this.selection.x1
            var factorWidth = 160/x
            var factorHeight = 120/y

            this.model.save({factorWidth: factorWidth, factorHeight:factorHeight, imageWidth: this.$('#logo').width(), imageHeight: this.$('#logo').height()})

            console.log('this.model', this.model);

            this.$('.imgDiv').css({height: 120, width: 160})

            this.$('.picture').attr('src', this.model.get('main_logo').url);

            this.$('.picture').css({width:this.$('#logo').width()*factorWidth, height:this.$('#logo').height()*factorHeight, top:0-this.selection.y1-((this.selection.y1*factorHeight-this.selection.y1)).toFixed(0), left: 0-this.selection.x1-((this.selection.x1*factorWidth-this.selection.x1)).toFixed(0)})
        }else{
            this.$('.picture').attr('src', this.model.get('main_logo').url);
        }
        this.$('#imagesModal').modal('hide')
    },

    _initDatepicker: function() {
        this.$('#dp2').datepicker();
        var startDate = new Date(2012,1,20);
        var endDate = new Date(2014,1,25);

        this.$('#dp4').datepicker().on('changeDate', function(ev){
            if (ev.date.valueOf() > endDate.valueOf()){
                $('#alert').show().find('strong').text('The start date can not be greater then the end date');
            } else {
                $('#alert').hide();
                startDate = new Date(ev.date);
                $('#startDate').text($('#dp4').data('date'));
            }
            $('#dp4').datepicker('hide');
        });
        
        this.$('#dp5').datepicker().on('changeDate', function(ev){
            if (ev.date.valueOf() < startDate.valueOf()){
                $('#alert').show().find('strong').text('The end date can not be less then the start date');
            } else {
                $('#alert').hide();
                endDate = new Date(ev.date);
                $('#endDate').text($('#dp5').data('date'));
            }
            $('#dp5').datepicker('hide');
        }); 
    }
    
});
