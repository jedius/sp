window.PasswordResetView = Backbone.View.extend({
	//events:{
		//'submit form.signin': 'signIn'
	//},
    
    initialize:function () {
        this.render();
    },

    render:function () {
        $(this.el).html(this.template('PasswordResetView'));
        return this;
    },
})
