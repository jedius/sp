window.ImageListView = Backbone.View.extend({

    events:{
        'click #open-add-image-modal': 'openAddModal',
        'change #files': 'imgDownload',
        //'click #add-image': 'add',
        // 'click #make-special-logo': 'makelogo',
        // 'click #saveArrea': 'saveArrea'
    },

    imgDownload: function(evt) {
        var self = this
        console.log('imgDownload');
        var files = evt.target.files;

        for (var i = 0, f; f = files[i]; i++) {
          // Only process image files.
          if (!f.type.match('image.*')) {
            continue;
          }
          var reader = new FileReader();
          // Closure to capture the file information.
          reader.onload = (function(theFile) {
            return function(e) {
                console.log('theFile', e.target.result);


            $.ajax({
                url: '/storage/upload',
                type: 'POST',
                data:  {
                    data: e.target.result,
                    file: JSON.stringify(theFile)
                },
                success: function(res) {
                    console.log('res',res);


                    if (res && res.url) {
                        self.model.get('images').push(res)
                        self.model.save()
                        console.log('self.model', self.model);
                        self.collection = new ImageCollection(self.model.get('images'));
                        self.render();
    
                        //console.log('modal', $('#imagesModal'));
                        //self.$('#imagesModal').modal('show')
                        
                        //self.$('.images-thumb').html('<img id="logo" src='+res.url+'></img>')
                        //self.$('#logo').imgAreaSelect({ aspectRatio: '4:3', handles: true, onSelectEnd: function (img, selection) {
                            //self.selection = selection
                            //console.log(self.selection, 'selection');

                        //} });    // work example

                        //self.model.setLogo(res.url);
                        //self.model.addImg(res.url);
                        //console.log('model please', self.model);
                        // self.$('.picture').attr('src', res.url);
                    }
                }
            });




              //Render thumbnail.

              //var span = document.createElement('span');
              //span.innerHTML = ['<img class="thumb" src="', e.target.result,
                                //'" title="', escape(theFile.name), '"/>'].join('');
              //document.getElementById('list').insertBefore(span, null);
            };
          })(f);
          reader.readAsDataURL(f);
        }
    },

    initialize: function () {
        this.collection = new ImageCollection(this.model.get('images'));
        this.collection.bind("add", this.update, this);
        this.render();
    },

    saveArrea: function  () {

        console.log('save');
        // body...
    },

    render: function () {
        console.log('render ImageListView');
        
        // console.log('coll', this.collection);
        

        $(this.el).html(this.template());
        $('#add-image-modal').modal();

        this.update();

        // $(this.el).append(new Paginator({collection: this.collection, page: this.options.page, count: count, entity: 'locations' }).render().el);

        return this;

        
        // $(this.el).html('<ul class="thumbnails"></ul>');
        // console.log('locations', locations);
        // for (var i = 0; i < locations.length; i++) {
        //   //  $('.thumbnails', this.el).append('ddd');
        //   $('.thumbnails', this.el).append(new LocationListItemView({model: locations[i]}).render().el);
        // }

        // $(this.el).append(new Paginator({model: this.model, page: this.options.page, count: count, entity: 'locations' }).render().el);

        // return this;
    },

    update: function () {
        var self = this;
        $('.thumbnails', self.el).html('');
        console.log('this.model', this.model);
        console.log('self.collection', self.collection);
        this.collection.each(function(model, i) {
            console.log(model,i);
            // model.set('picture', 'default.jpg');
            console.log('here', self.model);
            $('.thumbnails', self.el).append(new ImageListItemView({parentModel:self.model, model: model}).render().el);
            // $('.thumbnails', this.el).append(new LocationListItemView({model: locations[i]}).render().el);
        })
    },

    openAddModal: function (e) {
        $('#add-image-modal').modal('show');
    },

    add: function (e) {
        var url = this.$el.find('#image-url').val()

        if (url == '') return;

        this.model.addImg(url);
        this.model.downloadImg(url)
        this.collection.add({ url:url })
        $('#add-image-modal').modal('hide');
    }, 

    // back: function () {
    //     history.go(-1);
    // }

});

window.ImageListItemView = Backbone.View.extend({

    events:{
        'click #change-special-image': 'change',
        'click #delete-special-image': 'delete',
        'click #saveArrea': 'saveArrea',
        'click #make-special-logo': 'makelogo'
    },

    tagName: "li",

    initialize: function () {
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.remove, this);

        // console.log('url url',this.model.get('url'));
    },

    render: function () {
        console.log('render ImageListItemView');
        // console.log(this.template);
        // console.log(this.model);
        // console.log(this.model.toJSON());


        $(this.el).html(this.template(this.model.toJSON()));

        //fix interface
        console.log('URL LOGO!!!', this.model.get('url'),  this.options.parentModel.get('main_logo').url);
        if (this.model.get('url') === this.options.parentModel.get('main_logo').url) {
            $('.special-logo-picker').removeClass('disabled btn-primary');
            this.$el.find('#make-special-logo').addClass('disabled btn-primary');
        }

        return this;
    },

    makelogo: function () {
        console.log('makelogo');
        console.log('modal', $('#modalForImg'));
        self = this
        console.log("thumbnail", this.$('.thumbnail'));
        this.$('.thumbnail').append('<div id="modalForImg" class="modal hide fade"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3>click and select the viewing area</h3></div><div class="modal-body"><div class="images-thumb" align="center"></div></div><div class="modal-footer"><input id="saveArrea" class="btn saveArrea" value="save" type="button" /></div></div>')
        this.$('#modalForImg').modal('show')
        this.$('.images-thumb').html('<img id="logo" src='+this.model.get('url')+'></img>')
        this.$('#logo').imgAreaSelect({ aspectRatio: '4:3', handles: true, onSelectEnd: function (img, selection) {
            self.selection = selection
            console.log(selection, 'selection');
        } });    // work example


        // this.options.parentModel.setLogo(this.model.get('url'));

        // console.log(this.$el.find('#make-special-logo'));
        // $('.special-logo-picker').removeClass('disabled btn-primary');
        // this.$el.find('#make-special-logo').addClass('disabled btn-primary');
        
        // body...
    },

    saveArrea: function (e) {
        console.log('save');
        $('.imgareaselect-outer').hide()
        $('.imgareaselect-selection').parent().hide()
        console.log('this.model', this.options.parentModel);
        if(this.selection){
        //     // this.$('.picture').attr('src', res.url);
            var y = this.selection.y2-this.selection.y1
            var x = this.selection.x2-this.selection.x1
            var factorWidth = 160/x
            var factorHeight = 120/y
            console.log('y', y, 'x', x, 'factorWidth', factorWidth, 'factorHeight', factorHeight);
  
            // this.options.parentModel.save({main_logo:{url: this.model.get('url'), x1: this.selection.x1, x2: this.selection.x2, y1: this.selection.y1, y2: this.selection.y2}, factorWidth: factorWidth, factorHeight:factorHeight, imageWidth: this.$('#logo').width(), imageHeight: this.$('#logo').height()})
            console.log('this.options.parentModel', this.options.parentModel);
            // this.$('.imgDiv').css({height: 120, width: 160})
            this.options.parentModel.save({'imageWidth': this.$('#logo').width(), 'imageHeight': this.$('#logo').height(),  'factorWidth': factorWidth, 'factorHeight': factorHeight, 'main_logo':{'url':this.model.get('url'), 'x1':this.selection.x1, 'y1':this.selection.y1, 'x2':this.selection.x2, 'y2':this.selection.y2}})
            console.log('this.options.parentModel', this.options.parentModel);
            // this.$('.picture').attr('src', this.model.get('main_logo').url);

            // this.$('.picture').css({width:this.$('#logo').width()*factorWidth, height:this.$('#logo').height()*factorHeight, top:0-this.selection.y1-((this.selection.y1*factorHeight-this.selection.y1)).toFixed(0), left: 0-this.selection.x1-((this.selection.x1*factorWidth-this.selection.x1)).toFixed(0)})
        }else{
            var factorWidth = 160/this.$('#logo').width()
            var factorHeight = 120/this.$('#logo').height()
            this.options.parentModel.save({'imageWidth': this.$('#logo').width(), 'imageHeight': this.$('#logo').height(), 'main_logo':{'url':this.model.get('url'),x1:0, y1:0, x2:this.$('#logo').width(), y2:this.$('#logo').height()}, factorWidth: factorWidth, factorHeight: factorHeight})
        }
        // this.options.parentModel.setLogo(this.model.get('url'));
        $('.special-logo-picker').removeClass('disabled btn-primary');
        this.$el.find('#make-special-logo').addClass('disabled btn-primary');

        this.$('#modalForImg').modal('hide')
        this.$('#modalForImg').remove()

        e.preventDefault()
        // body...
    },

    change: function () {
        console.log('change');
        // body...
    },

    delete: function () {
        console.log('delete');
        
        // console.log(this);
        console.log("this.model.get('url')", this.model.get('url'));
        this.options.parentModel.deleteLogo(this.model.get('url'));
        this.model.destroy();
        //this.remove();
        // body...
    },



});
