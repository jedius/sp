window.HeaderView = Backbone.View.extend({

    initialize: function () {
        //this.render()
    },

    render: function () {
        var name
        // console.log('window.user.get', window.user.get('email'));
        // console.log('window.user.get', window.user.get('firstName'));
        console.log('render HeaderView');
        if(window.user.get('firstName')){
            name = window.user.get('firstName')
        }


        $('.header').html(this.template({
            name: name
        }));
        return this;
    },

    selectMenuItem: function (menuItem) {
        $('.nav li').removeClass('active');
        if (menuItem) {
            $('.' + menuItem).addClass('active');
        }
    }
});
