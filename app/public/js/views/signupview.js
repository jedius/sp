window.SignUpView = Backbone.View.extend({

    events:{
        "submit form.signup-form": "join",
        'click .male': 'chooseMail',
        'click .female': 'chooseFemail'
    },

    initialize:function () {
        this.render();
    },

    render:function () {
        $(this.el).html(this.template('SignUpView'));
        return this;
    },

    join:function(e) {
        var data = this.$('.signup-form').serializeObject()
        if(this.$('.male').hasClass('disabled')){
            data.gen='male'
        }else{
            if(this.$('.female').hasClass('disabled')){
                data.gen='female'
            }
        }
        if(data.gen){
            console.log('data.password', data.password, this.$('.confirmPassword').val());
            if(data.password == this.$('.confirmPassword').val()){
                window.user.join(data, function(err) {
                    if(!err){
                        utils.showAlert('Success!', 'Join success', 'alert-success');
                        app.navigate('/', {trigger: true});
                        //window.user.on('change',function(){
                            //console.log('change');
                            app.headerView.render()
                        //})

                    }else{
                        utils.showAlert('Error', err, 'alert-error');
                        //this.$('.errSignUp').text(err)
                    }
                })
            }else{
                utils.showAlert('Error', 'error password confirmation', 'alert-error');
            }
        }else{
            utils.showAlert('Warning!', 'Please select your gender', 'alert-warn');
        }

    	e.preventDefault()
    	return false
    },

    chooseMail:function(e) {
        console.log('chooseMail');
        this.$('.male').addClass('disabled')
        this.$('.female').removeClass('disabled')
    	e.preventDefault()
    	return false
    },

    chooseFemail:function(e) {
        console.log('chooseFEmail');
        this.$('.male').removeClass('disabled')
        this.$('.female').addClass('disabled')
    	e.preventDefault()
    	return false
    }


});
