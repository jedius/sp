window.UserModel = Backbone.Model.extend({
    initialize: function () {
        console.log('unit UserModel');
    },

    login:function (data,cb) {
        var self = this;
        $.ajax({
            url:'/login',
            type:'POST',
            data: data,
            error:function(err) {
            },
            success:function(res){
                switch(res.code){
                    case 200:
                        self.set(res.data);
                        cb()
                        break;

                    case 400:
                        cb(res.error)
                        break;

                    default:
                        cb(res.error)
                        break;
                }  
            }
        })
    },

  join:function (data,cb) {
        var self = this;
    $.ajax({
      url:'/join',
      type:'POST',
      data: data,
      success:function(res){
                switch(res.code){
                    case 200:
                        //console.log('res.data:');
                        //console.log(res.data);
                        window.user.set(data);
                        //console.log('triggering...');
                        //window.user.trigger('change');
                        cb();
                        break;

                    case 400:
                        cb(res.error);
                        break;

                    default:
                        cb(res.error);
                        break;
                }
      }
    });
  },

    logOut: function (cb) {
        console.log('logout model');
        $.ajax({
            url: '/logout',
            type: 'POST',
            success: function() {
                window.user.clear()
                location.href = '/';
                cb()
            }
        });
    },

    update: function (data, cb) {
        var self = this;
        $.ajax({
            url: '/user/'+ window.user.get('id'),
            data: data,
            type: 'POST',
            success: function(res){
                switch(res.code){
                    case 200:
                        window.user.set(data);
                        cb();
                        break

                    case 400:
                        cb(res.error)
                        break

                    default:
                        cb(res.error)
                        break
                }  
                
            }
        });
    },

    findSelf: function (cb) {
        var self = this;
        $.ajax({
            url: '/user/',
            type: 'GET',
            success: function(res){
                console.log('res', res);
                switch(res.code){
                    case 200:
                        window.user.set(res.data)
                        cb()
                        break

                    case 400:
                        console.log('error, 400', res.error);
                        cb()
                        break

                    default:
                        console.log('error', res.error);
                        cb()
                        break
                }
            }
        });
    }
})