window.Special = Backbone.Model.extend({

    // url: function () {
    //     return "/specials/" + this.get("_id"); 
    // },

    url: function () {
        id = this.get("_id");
        if (id) {
            return "/specials/" + this.get("_id");
        } else {
            return "/specials";
        }
    },


    idAttribute: "_id",

    initialize: function () {

        this.validators = {};
        // this.relatedLocations = new window.LocationLinkCollection()
        // this.relatedLocations.fetchId = this.get('_id');

        this.validators.name = function (value) {
            return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a name"};
        };

        // this.validators.grapes = function (value) {
        //     return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a grape variety"};
        // };

        //this.validators.country = function (value) {
        //    return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a country"};
        // };

        // console.log(this.get('images'));
        // console.log(this);
        // console.log(this.get('images').indexOf(this.main_logo));
    },

    _getRelatedLocationsIds: function (_param) {
        if (this.get("_id")) {
            var self = this;
            $.ajax({
                url:'/specials/getRelatedLocationsIds/' + self.get('_id'),
                type: 'GET',
                error:function(err) {
                
                },
                success:function(res, body, xhr){
                    console.log(arguments);
                    console.log('res', xhr.status);
                    switch(xhr.status){
                        case 200:
                            console.log(200);
                            self.set('location_id', res);
                            //var specials = _.map(res, function(num){ return { _id: num } });
                            // for(var i=0; i<res.specials.length; i++){
                            //     self.add(res.specials[i]);
                            // }
                            _param.success();
                            break;

                        case 400:
                            // _param.error(res.error)
                            break;

                        default:
                            console.log('res');
                            //cb(res.error)
                            break;
                    }  
                }
            });
        } else {
            _param.success();
        }
        
    },

    validateItem: function (key) {
        return (this.validators[key]) ? this.validators[key](this.get(key)) : {isValid: true};
    },

    // TODO: Implement Backbone's standard validate() method instead.
    validateAll: function () {

        var messages = {};
        console.log('this', this);

        for (var key in this.validators) {
            if(this.validators.hasOwnProperty(key)) {
                var check = this.validators[key](this.get(key));
                console.log('check', check);
                if (check.isValid === false) {
                    messages[key] = check.message;
                }
            }
        }

        return _.size(messages) > 0 ? {isValid: false, messages: messages} : {isValid: true};
    },

    setRelatedLocations: function (arr) {
        // console.group('setRelatedLocations');
        // console.log('arr', arr.length);
        var self = this;

        this.saveRelatedLocations({
            location_ids: arr,
            success: function() {
                // console.log('123');
                console.log('save special_ids to model', arr);
                self.save({location_id: arr});
            }
        });
        // this.save({location_id: arr});
        // console.log('save locations');
        // console.groupEnd();
    },

    saveRelatedLocations: function (_param) {
        console.group('saveRelatedLocations');
        console.log('arguments', arguments);

        // _param.special_ids
        var self = this

        var data = {
            location_ids: _param.location_ids
        };

        console.log('send data', data);

        $.ajax({
            url:'/specials/saveRelatedLocations/' + self.get('_id'),
            type:'POST',
            data: data,
            error:function(err) {

            },
            success:function(res){
                console.log('res', res);
                console.groupEnd();
                switch(res.status){
                    case 200:
                        console.log(200);

                        _param.success(res.locations)
                        break;

                    case 400:
                        // _param.error(res.error)
                        break;

                    default:
                        console.log('res');
                        //cb(res.error)
                        break;
                }  
            }
        });

    },



    // addRelatedLocation: function (_param) {
    //     console.log('addRelatedSpecial', arguments);
    //     var self = this
    //     $.ajax({
    //         url:'/specials/addRelatedLocation/'+ _param.loc +'/to/'+ self.get('_id'),
    //         type:'GET',
    //         error:function(err) {
    //         },
    //         success:function(res){
    //             console.log('res', res);
    //             switch(res.status){
    //                 case 200:
    //                     console.log(200);
    //                     // for(var i=0; i<res.specials.length; i++){
    //                     //     self.add(res.specials[i]);
    //                     // }
    //                     _param.success(res.specials)
    //                     break;

    //                 case 400:
    //                     // _param.error(res.error)
    //                     break;

    //                 default:
    //                     console.log('res');
    //                     //cb(res.error)
    //                     break;
    //             }  
    //         }
    //     });
    // },

    addImg: function (url) {
        console.log('special, add image');
        console.log('before', this);
        var arr = this.get('images') || [];
        arr.push({ url: url });
        //this.save({ images: arr });

        console.log('images', this);
    },

    downloadImg:function(url) {
        $.ajax({
            url:'/storage/upload/'+url,
            type: 'POST',
            error:function(err) {
                console.log('error');
            },
            success:function(res){
                console.log('res', res);
                //switch(xhr.status){
                    //case 200:
                        //console.log(200);
                        //self.set('location_id', res);
                        //_param.success();
                        //break;
                    //default:
                        //console.log('res');
                        ////cb(res.error)
                        //break;
                //}  
            }
        });
        console.log('downloadImg');
    },


    deleteLogo: function (url) {
        console.log('special, add image');
        console.log('before', this);
        var arr = this.get('images') || [];
        console.log(this, 'this');
        console.log('arr', arr);
        console.log('url', url);
        for( var i = 0 ; i < arr.length; i++){
            if(arr[i].url == url){
                console.log('here', i);
                arr.splice(i,1);
            }
        }
        var index = arr.indexOf({ url: url });
        console.log('index', index);
        if(url === this.get('main_logo').url){
            this.set({main_logo: {url: "", x1:0, y1:0}, imageHeight: 120, imageWidth: 160, factorHeight:1, factorWidth:1})
        }
        this.save({ images: arr });

        console.log('after', this);
    },

    setLogo: function (url) {
        // console.log('special, add image');
        // console.log('before', this);
        // var arr = this.get('logos') || [];
        // arr.push({ url: url });
        this.save({ main_logo: { url: url, x1:0, y1:0}, imageHeight: 120, imageWidth: 160, factorHeight:1, factorWidth:1 });

        // console.log('after', this);
    },

    defaults: {
        _id: null,
        name: "",
        location_id: [],
        main_logo: { x1:0, y1:0, y2:120, x2:160 },
        images: [],
        description: "",
        condition: "",
        mon: "",
        monEnd: "",
        tues: "",
        tuesEnd: "",
        wed: "",
        wedEnd: "",
        thur: "",
        thurEnd: "",
        fri: "",
        friEnd: "",
        sat: "",
        satEnd: "",
        sun: "",
        sunEnd: "",
        pub: "",
        pubEnd: "",
        imageWidth: 160,
        imageHeight: 120,
        factorWidth: 1,
        factorHeight: 1,
        price: ""

    }
});
