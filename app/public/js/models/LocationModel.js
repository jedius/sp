window.Location = Backbone.Model.extend({

    url: function () {
        if (this.get("_id")) {
            return "/locations/" + this.get("_id");
        } else {
            return "/locations";
        }
    },

    idAttribute: "_id",

    initialize: function () {

        this.validators = {};
        // this.on('fetch', this.relatedSpecials.fetch(), this)

        this.validators.name = function (value) {
            return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a name"};
        };

       // this.validators.grapes = function (value) {
       //     return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a grape variety"};
       // };

        //this.validators.country = function (value) {
        //    return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a country"};
       // };
    },

    setRelatedSpecials: function (arr) {
        // console.group('setRelatedLocations');
        // console.log('arr', arr.length);
        var self = this
        this.saveRelatedSpecials({
            special_ids: arr,
            success: function() {
                // console.log('123');
                console.log('save special_ids to model', arr);
                self.save({special_id: arr});
            }
        });
        // console.log('save locations');
        // console.groupEnd();
    },

    saveRelatedSpecials: function (_param) {
        console.group('saveRelatedSpecials');
        console.log('arguments', arguments);

        // _par.special_ids
        var self = this

        var data = {
            special_ids: _param.special_ids
        };

        console.log('send data', data);

        $.ajax({
            url:'/locations/saveRelatedSpecials/' + self.get('_id'),
            type:'POST',
            data: data,
            error:function(err) {

            },
            success:function(res){
                console.log('res', res);
                console.groupEnd();
                switch(res.status){
                    case 200:
                        console.log(200);
                        // for(var i=0; i<res.specials.length; i++){
                        //     self.add(res.specials[i]);
                        // }
                        _param.success(res.specials)
                        break;

                    case 400:
                        // _param.error(res.error)
                        break;

                    default:
                        console.log('res');
                        //cb(res.error)
                        break;
                }  
            }
        });

    },
    
    validateItem: function (key) {
        return (this.validators[key]) ? this.validators[key](this.get(key)) : {isValid: true};
    },

    // TODO: Implement Backbone's standard validate() method instead.
    validateAll: function () {

        var messages = {};

        for (var key in this.validators) {
            if(this.validators.hasOwnProperty(key)) {
                var check = this.validators[key](this.get(key));
                if (check.isValid === false) {
                    messages[key] = check.message;
                }
            }
        }

        return _.size(messages) > 0 ? {isValid: false, messages: messages} : {isValid: true};
    },

    makePublic: function (url) {
        var arr = this.get('public_images') || [];
        arr.push({ url: url });
        this.save({ public_images: arr });
    },

    addImg: function (url) {
        console.log('special, add image');
        console.log('before', this);
        var arr = this.get('images') || [];
        arr.push({ url: url });
        console.log('arr', arr);

        //this.save({ images: arr });

        console.log('after', this);
    },

    setLogo: function (url) {
        // console.log('special, add image');
        // console.log('before', this);
        // var arr = this.get('logos') || [];
        // arr.push({ url: url });
        console.log('setLogo');
        this.save({ main_logo: {url: url} });
        console.log('location model', this);
        // console.log('after', this);
    },

    _getRelatedSpecialsIds: function (_param) {
        if (this.get("_id")) {
            var self = this;
            $.ajax({
                url:'/locations/getRelatedSpecialsIds/' + self.get('_id'),
                type: 'GET',
                error:function(err) {
                
                },
                success:function(res, body, xhr){
                    console.log(arguments);
                    console.log('res', xhr.status);
                    switch(xhr.status){
                        case 200:
                            console.log(200);
                            self.save({ special_id: res });
                            //var specials = _.map(res, function(num){ return { _id: num } });
                            // for(var i=0; i<res.specials.length; i++){
                            //     self.add(res.specials[i]);
                            // }
                            _param.success();
                            break;

                        case 400:
                            // _param.error(res.error)
                            break;

                        default:
                            console.log('res');
                            //cb(res.error)
                            break;
                    }  
                }
            });
        } else {
            _param.success();
        }
        
    },

    defaults: {     
        _id : null,
        name: "",
        main_logo: { x1:0, y1:0, y2:120, x2:160 },
        public_images: [], // shown for users
        images: [], //all images
        email : "",
        special_id: [],
        phone : "",
        mon: "",
        monEnd: "",
        tues: "",
        tuesEnd: "",
        wed: "",
        wedEnd: "",
        thur: "",
        thurEnd: "",
        fri: "",
        friEnd: "",
        sat: "",
        satEnd: "",
        sun: "",
        sunEnd: "",
        pub: "",
        pubEnd: "",
        address : "",
        loc : "",
        url : "",
        imageWidth: 160,
        imageHeight: 120,
        factorWidth: 1,
        factorHeight: 1,
        category : "",
        external_id : null
            
    }
});
