var express;

global.glob = {};

glob.modules = {
    express: express = require('express'),
    path: require('path'),
    http: require('http'),
    fs: require('fs'),
    mongoose: require('mongoose'),
    redis: require('redis'),
    passport: require('passport'),
    passportLocal: require('passport-local'),
    crypto: require('crypto')
};

var env = process.env.NODE_ENV || 'development';
glob.config = require('../config')(env);

//mongo
global.mongo = require('./mongo');





//redis
glob.redis = glob.modules.redis.createClient(glob.config.db.redisURL.port, glob.config.db.redisURL.hostname);
if (glob.config.db.redisURL.auth) {
  glob.redis.auth(glob.config.db.redisURL.auth.split(":")[1]);
}
var sessionStore = require('connect-redis')(express);
glob.sessionStore = new sessionStore({
  client: glob.redis,
  prefix: 'wineCellar:'
});

var app = express();
app.configure(function() {
    app.set('ip', process.env.IP);
    app.set('port', process.env.PORT || 3000);
    app.set('views', __dirname + '/public');
    app.set('env',env);
    app.use(express.cookieParser());
    app.use(express.session({
      secret: glob.config.app.secretString,
      store: glob.sessionStore,
      cookie: {
        maxAge: glob.config.app.maxAge
      }
    }));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.set('view engine', 'jade');
    app.use(glob.modules.passport.initialize());
    app.use(glob.modules.passport.session());
    require ('./passport.js')
    app.use(app.router);
    app.use(express.static(glob.modules.path.join(__dirname, 'public')));
});

app.enable("jsonp callback"); 

var httpServer = glob.modules.http.createServer(app).listen(process.env.PORT || 1337, process.env.IP || '127.0.0.1');

require('./router.js')(app);


try {
  glob.modules.fs.mkdirSync(glob.config.db.images)  
} catch (e) {}


glob.modules.mongoose.connect(glob.config.db.mongoUrl, function(err) {
  if (err) {
    console.log('mongo err:');
    console.log(err);
  } else {
    httpServer.listen(app.get('port'), function () {
        console.log("Express server listening on port " + app.get('port') + ' in ' + app.settings.env + ' mode');
    });    
  }
});

// function a(argument) {
//     console.log('12312312312312313132231');
//     mongo.spec_loc_rel.find({},function(err,s) {
//         console.log(s);    
        
//     });
// }

// a();


// io = require('socket.io'),

// io = io.listen(server);

// io.configure(function () {
//     io.set('authorization', function (handshakeData, callback) {
//         if (handshakeData.xdomain) {
//             callback('Cross-domain connections are not allowed');
//         } else {
//             callback(null, true);
//         }
//     });
// });

// io.sockets.on('connection', function (socket) {

//     socket.on('message', function (message) {
//         console.log("Got message: " + message);
//         ip = socket.handshake.address.address;
//         url = message;
//         io.sockets.emit('pageview', { 'connections': Object.keys(io.connected).length, 'ip': '***.***.***.' + ip.substring(ip.lastIndexOf('.') + 1), 'url': url, 'xdomain': socket.handshake.xdomain, 'timestamp': new Date()});
//     });

//     socket.on('disconnect', function () {
//         console.log("Socket disconnected");
//         io.sockets.emit('pageview', { 'connections': Object.keys(io.connected).length});
//     });

// });