var logFrom, passport, LocalStrategy;

var crypt = function(str) {
  var cipher = glob.modules.crypto.createCipher('aes-256-cbc', glob.config.app.secretString)
  var crypted = cipher.update(str,'utf8','hex')
  return crypted += cipher.final('hex')
}
  

var decrypt = function(str) {
  try {
    var decipher = glob.modules.crypto.createDecipher('aes-256-cbc',glob.config.app.secretString)
    var decrypted = decipher.update(str,'hex','utf8')
    return decrypted += decipher.final('utf8')
  } catch (err) {
    log.warn(err,'utils.decrypt')
    return false
  }
}
  


passport = glob.modules.passport;
LocalStrategy = glob.modules.passportLocal.Strategy;
passport.serializeUser(function(user, done) {
  if (user.id) {
    done(null, user.id);
  }
});

passport.deserializeUser(function(id, done) {
  if (id) {
    mongo.user.findById(id, function(err, user) {
      if (!err) {
        if (user) {
          done(null, user.entity)
        } else {
          done(null, false)
        }
      } else {
        done(err, false)
      }
    });
  } else {
  }
});

passport.use('local-login', new LocalStrategy(function(username, password, done) {
  mongo.user.findOne({
    email: username
  }, function(err, user) {
    if (!err) {
      if (user) {
        console.log('body.pass', password);
        console.log('user.pass', user.password);
        console.log('crypted body pass', crypt(password));
        console.log('user: ', user);
        if (user.password == crypt(password)) {
          console.log('hello: ',user.entity);
          done(null, user.entity);
        } else {
          done(err, false);
        }
      } else {
        done(err, false);
      }
    } else {
      done(err, false);
    }
  });
}))

passport.use('local-join', new LocalStrategy(function(username, password, done) {
  if (username, password) {
    mongo.user.findOne({
      email: username
    }, function(err, user) {
      if (!err) {
        if (!user) {
          var nUser = new mongo.user({
            email: username,
            password: crypt(password),
            dt: new Date(),
            lm: new Date()
          }).save(function(err, user) {
            if (!err) {
              if (user) {
                console.log("created user", user);
                done(null, user.entity, 'success');
              } else {
                done(err, false, 'err');
              }
            } else {
              done(err, false, {
                message: 'user already registred'
              });
            }
          });
        } else {
          return done(null, false, {
            message: 'user already registred'
          });
        };
      } else {
        return done(err, false, {
          message: 'user already registred'
        });
      }
    });
  } else {
    return done(null, false, {
      message: 'bad credentials'
    })
  }
}))
