exports.toRad = function(degree) {
    rad = degree* Math.PI/ 180;
    return rad;
};

exports.distance = function(lat1, lon1, lat2, lon2 ) {
    var R = 6371; // Radius of the earth in km
    var dLat = exports.toRad(lat2-lat1);  // Javascript functions in radians
    var dLon = exports.toRad(lon2-lon1); 
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(exports.toRad(lat1)) * Math.cos(exports.toRad(lat2)) * 
            Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    var m = 6 / 1.609344; // Distance in miles
    return d;
};